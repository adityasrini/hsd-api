# Welcome to DC's Human Services Directory API! [![pipeline status](https://gitlab.com/adityasrini/hsd-api/badges/dev/pipeline.svg)](https://gitlab.com/adityasrini/hsd-api/commits/dev) [![coverage report](https://gitlab.com/adityasrini/hsd-api/badges/dev/coverage.svg)](https://gitlab.com/adityasrini/hsd-api/commits/dev) <a width="150" height="50" href="https://auth0.com/?utm_source=oss&utm_medium=gp&utm_campaign=oss" target="_blank" alt="Single Sign On & Token Based Authentication - Auth0">![](https://cdn.auth0.com/oss/badges/a0-badge-light.png)</a>

This is the source code for DC's Human Services Directory. This project will closely match the published API endpoints of the [HSDA standard by Open Referral](https://openreferral.readthedocs.io/en/latest/hsda/hsda/).

To start development:
- Download the latest version of open jdk
- Download and install Postgres
- Clone this repo. `cd` into it from the terminal.
- Create an environment settings file and call it `.env`. Alternatively, you may edit your `.bash_profile` file to include the following shell commands.
```
export HOMELESS_DB_URL=postgresql://localhost:5432/{DATABASE WHERE YOU WILL STORE THE HOMELESS SERVICES DIRECTORY}
export HOMELESS_DB_PASSWORD={YOUR DATABASE USERNAME}
export HOMELESS_DB_USERNAME={YOUR DATABASE PASSWORD}
```
- run `source .env` (alternatively `source .bash_profile` if you added these variables to your `.bash_profile`)
- run `./gradlew flywayMigrate` to execute the database migrations with seed data.

To run this app:
 - Run `./gradlew run`

If you encounter a bug, open an issue here.
If you encounter security issues, please directly email me: <aditya dot s at outlook dot com> (replace the `dot` and `at` with symbols. No spaces.)
**Please do not publicize security issues!**

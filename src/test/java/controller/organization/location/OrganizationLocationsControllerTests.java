package controller.organization.location;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

@MicronautTest
class OrganizationLocationsControllerTests {
  private static String organizationOne = "44EEB4B4-48C6-9BD6-9429-454ED533353B";
  private static String locationOneOrganizationOne = "0B107868-996B-6160-3EDA-32570BC139B2";
  private static String nonExistentLocation = "DOESNOTEXIST";
  private static String organizationTwo = "7322ECBB-628C-20BF-91B9-F5AE907C69EF";

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetLocationsForOrganization() {
    var responseStatus = client.toBlocking()
        .exchange("/organizations/" + organizationOne + "/locations").status();

    assertEquals(HttpStatus.OK, responseStatus);
  }


  @Test
  void canGetOneLocationForOrganization() throws JSONException {
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/" + organizationOne + "/locations/" + locationOneOrganizationOne,
            ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString = "{organization_id: \"" + organizationOne
        + "\", id: \"" + locationOneOrganizationOne + "\"}";

    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }


  @Test
  void cannotGetUnknownLocationForOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationOne + "/locations/" + nonExistentLocation,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetUnknownLocationForUnknownOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/RANDOM-ORGANIZATION/locations/" + nonExistentLocation,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetLocationBelongingToDifferentOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationTwo + "/locations/" + locationOneOrganizationOne,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void getNotFoundExceptionForLocationOfEmptyOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .exchange("/organizations//locations/"));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }
}

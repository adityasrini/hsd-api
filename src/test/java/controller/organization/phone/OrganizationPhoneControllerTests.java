package controller.organization.phone;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

@MicronautTest
class OrganizationPhoneControllerTests {

  private static String organizationOne = "44EEB4B4-48C6-9BD6-9429-454ED533353B";
  private static String phoneOneOrganizationOne = "0CD1C3A0-8CC9-1413-5F03-897C30B22E6B";
  private static String nonExistentPhone = "DOESNOTEXIST";
  private static String organizationTwo = "7322ECBB-628C-20BF-91B9-F5AE907C69EF";

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetPhonesForOrganization() {
    var responseStatus = client.toBlocking()
        .exchange("/organizations/" + organizationOne + "/phones").status();

    assertEquals(HttpStatus.OK, responseStatus);
  }


  @Test
  void canGetOnePhoneForOrganization() throws JSONException {
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/" + organizationOne + "/phones/" + phoneOneOrganizationOne,
            ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString = "{organization_id: \"" + organizationOne
        + "\", id: \"" + phoneOneOrganizationOne + "\"}";

    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }


  @Test
  void cannotGetUnknownPhoneForOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationOne + "/phones/" + nonExistentPhone,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetUnknownPhoneForUnknownOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/RANDOM-ORGANIZATION/phones/" + nonExistentPhone,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetPhoneBelongingToDifferentOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationTwo + "/phones/" + phoneOneOrganizationOne,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void getNotFoundExceptionForPhoneOfEmptyOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .exchange("/organizations//phones/"));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }
}

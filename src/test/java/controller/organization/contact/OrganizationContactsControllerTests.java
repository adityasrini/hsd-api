package controller.organization.contact;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

@MicronautTest
class OrganizationContactsControllerTests {

  private static String organizationOneId = "C58FDCA7-8529-5B92-93CB-F80CE0D0330C";
  private static String orgOneContactId = "4F515802-906E-495F-94A6-9767F3CEB6F7";
  private static String orgTwoContactIdTwo = "4F515802-906E-495F-94A6-9767F3CEB6F8";
  private static String nonExistentContact = "DOESNOTEXIST";
  private static String nonExistentOrganization = "DOESNOTEXIST";

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetIndexOfContactsForSpecificOrganization() {
    var responseStatus = client.toBlocking()
        .exchange("/organizations/" + organizationOneId + "/contacts").status();
    assertEquals(HttpStatus.OK, responseStatus);
  }


  @Test
  void canGetSpecificContactForSpecificOrganization() throws JSONException {
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/" + organizationOneId + "/contacts/" + orgOneContactId,
            ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString = "{email: \"no-reply@blah.com\", name: \"Person\"}";

    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }


  @Test
  void cannotGetUnknownContactOfSpecificOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationOneId + "/contacts/" + nonExistentContact,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetUnknownContactOfUnknownOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve(
                "/organizations/" + nonExistentOrganization + "/contacts/" + nonExistentContact,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetSpecificContactOfDifferentOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationOneId + "/contacts/" + orgTwoContactIdTwo,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }
}

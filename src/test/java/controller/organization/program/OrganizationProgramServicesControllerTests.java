package controller.organization.program;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

@MicronautTest
class OrganizationProgramServicesControllerTests {

  private static String organizationOne = "44EEB4B4-48C6-9BD6-9429-454ED533353B";
  private static String programOneOrganizationOne = "93AB3E78-4DF5-7242-5487-249126436F31";
  private static String programTwoOrganizationOne = "93AB3E78-4DF5-7242-5487-249126436F32";
  private static String serviceOneProgramOneOrganizationOne = "F2234502-6657-3778-9CFF-4FE911990783";
  private static String organizationTwo = "1F69897C-657A-80EC-19C3-1D4BEAB52C3D";
  private static String programTwoOrganizationTwo = "5A35D6EA-53AF-8F29-5DA9-7D17E67B74C7";
  private static String nonExistentOrganization = "NON-EXISTENT-ORGANIZATION";
  private static String nonExistentProgram = "NON-EXISTENT-PROGRAM";
  private static String nonExistentService = "NON-EXISTENT-SERVICE";

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetIndexOfServicesForProgramInOrganization() {
    var responseStatus = client.toBlocking()
        .exchange("/organizations/" + organizationOne + "/programs/" + programOneOrganizationOne
            + "/services").status();
    assertEquals(HttpStatus.OK, responseStatus);
  }

  @Test
  void canGetServiceForProgramInOrganization() throws JSONException {
    var locationOrganizationOne = "47C7062B-4F3D-9234-1D34-1CDFA4576A57";
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/" + organizationOne
                + "/programs/" + programOneOrganizationOne
                + "/services/" + serviceOneProgramOneOrganizationOne,
            ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString = "{ \"id\": \"" + serviceOneProgramOneOrganizationOne
        + "\", \"organization_id\": \"" + organizationOne + "\", \"program_id\": \""
        + programOneOrganizationOne
        + "\", \"location_id\": \"" + locationOrganizationOne + "\","
        + "\"name\": \"TRANSPORTATION\", \"alternate_name\": null, \"description\": null, "
        + "\"url\": \"https://dhs.dc.gov/service/hypo-hyperthermia-watch\", \"email\": null, "
        + "\"status\": \"STATUS UNKNOWN\", \"interpretation_services\": null, "
        + "\"application_process\": null, \"wait_time\": null, \"fees\": null, "
        + "\"accreditations\": null, \"licenses\": null }";

    JSONAssert.assertEquals(expectedString, jsonObject, true);
  }

  @Test
  void cannotGetUnknownServiceForProgramInOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationOne
                    + "/programs/" + programOneOrganizationOne
                    + "/services/" + nonExistentService,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetUnknownServiceForUnknownProgramInOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationOne
                    + "/programs/" + nonExistentProgram
                    + "/services/" + nonExistentService,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetUnknownServiceForUnknownProgramInUnknownOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + nonExistentOrganization
                    + "/programs/" + nonExistentProgram
                    + "/services/" + nonExistentService,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetServiceForProgramInDifferentOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationTwo
                    + "/programs/" + programOneOrganizationOne
                    + "/services/" + serviceOneProgramOneOrganizationOne,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetServiceForDifferentProgramInDifferentOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationTwo
                    + "/programs/" + programTwoOrganizationOne
                    + "/services/" + serviceOneProgramOneOrganizationOne,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetServiceForDifferentProgramInSameOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationOne
                    + "/programs/" + programTwoOrganizationTwo
                    + "/services/" + serviceOneProgramOneOrganizationOne,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }
}

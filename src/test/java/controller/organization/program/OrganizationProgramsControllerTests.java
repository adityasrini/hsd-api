package controller.organization.program;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

@MicronautTest
class OrganizationProgramsControllerTests {

  private static String organizationOne = "1F69897C-657A-80EC-19C3-1D4BEAB52C3D";
  private static String programOneOrganizationOne = "5A35D6EA-53AF-8F29-5DA9-7D17E67B74C7";
  private static String nonExistentProgram = "DOESNOTEXIST";
  private static String organizationTwo = "44EEB4B4-48C6-9BD6-9429-454ED533353B";

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetIndexOfProgramsForSpecificOrganization() {
    var responseStatus = client.toBlocking()
        .exchange("/organizations/" + organizationOne + "/programs").status();
    assertEquals(HttpStatus.OK, responseStatus);
  }


  @Test
  void canGetSpecificProgramForSpecificOrganization() throws JSONException {
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/" + organizationOne + "/programs/" + programOneOrganizationOne,
            ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString = "{organization_id: \"" + organizationOne
        + "\", name: \"Legal Assistance Project\"}";

    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }


  @Test
  void cannotGetUnknownProgramOfSpecificOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
        .retrieve("/organizations/" + organizationOne + "/programs/" + nonExistentProgram,
            ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetUnknownProgramOfUnknownOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
        .retrieve("/organizations/RANDOM-ORGANIZATION/programs/" + nonExistentProgram,
            ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetSpecificProgramOfDifferentOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
        .retrieve("/organizations/" + organizationTwo + "/programs/" + programOneOrganizationOne,
            ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }
}

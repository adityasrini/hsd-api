package controller.organization.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

@MicronautTest
class OrganizationServicesControllerTests {

  private static String organizationOne = "C58FDCA7-8529-5B92-93CB-F80CE0D0330C";
  private static String serviceOneOrganizationOne = "98AEDC3F-006B-8023-757F-EDB012EE4947";
  private static String serviceTwoOrganizationOne = "BE2CE8DD-0CAF-80E5-8558-4BD035E90AE5";
  private static String organizationTwo = "1F69897C-657A-80EC-19C3-1D4BEAB52C3D";

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetIndexOfServicesForSpecificOrganization() {
    var responseStatus = client.toBlocking()
        .exchange("/organizations/" + organizationOne + "/services").status();
    assertEquals(HttpStatus.OK, responseStatus);
  }


  @Test
  void canGetSpecificServiceForSpecificOrganization() throws JSONException {
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/" + organizationOne + "/services/" + serviceOneOrganizationOne,
            ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString = "{organization_id: \"" + organizationOne
        + "\", name: \"FOOD_GROCERIES\", url: \"http://www.anacostiaoutreach.org/wordpress/\" }";

    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }


  @Test
  void cannotGetUnknownServiceOfSpecificOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
        .retrieve("/organizations/" + organizationOne + "/services/NON-EXISTENT-SERVICE",
            ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetUnknownServiceOfUnknownOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
        .retrieve("/organizations/RANDOM-ORGANIZATION/services/NON-EXISTENT-SERVICE",
            ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetSpecificServiceOfDifferentOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
        .retrieve("/organizations/" + organizationOne + "/services/" + serviceTwoOrganizationOne,
            ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());

  }
}

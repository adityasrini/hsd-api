package controller.organization;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Objects;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

@MicronautTest
class OrganizationsControllerTests {

  private static String nonExistentOrg = "DOESNOTEXIST";

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetOrgIndexWithNoParams() {
    var response = client.toBlocking().exchange("/organizations/", ArrayList.class);
    assertEquals(HttpStatus.OK, response.getStatus());
    assertEquals(4, Objects.requireNonNull(response.body()).size());
  }

  @Test
  void canGetOrgWithQueryParam() {
    var query = "name";
    var queries = URLEncoder.encode("Department Of Human Services", StandardCharsets.UTF_8);
    var response = client.toBlocking()
        .exchange("/organizations?query=" + query + "&queries=" + queries, ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertEquals(1, Objects.requireNonNull(response.body()).size());
  }

  @Test
  void canGetOrgIndexWithPerPageParam() {
    var perPage = 2;
    var response = client.toBlocking()
        .exchange("/organizations?per_page=" + perPage, ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertEquals(2, Objects.requireNonNull(response.body()).size());
  }

  @Test
  void cannotGetOrgIndexWithMoreThan100PerPageParam() {
    var perPage = 1000;
    var response = client.toBlocking()
        .exchange("/organizations?per_page=" + perPage, ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertTrue(100 >= Objects.requireNonNull(response.body()).size());
  }

  @Test
  void canGetOrgIndexWithSortByParam() {
    var sortBy = "name";
    var response = client.toBlocking()
        .exchange("/organizations?sort_by=" + sortBy, ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertTrue(0 < Objects.requireNonNull(response.body()).size());
    var firstOrganization = (LinkedHashMap) Objects.requireNonNull(response.body()).get(0);
    assertEquals("Anacostia Community Outreach Center (ACOC)", firstOrganization.get("name"));
  }

  @Test
  void canGetOrgIndexWithOrderParam() {
    var order = "desc";
    var sortBy = "name";
    var response = client.toBlocking()
        .exchange("/organizations?sort_by=" + sortBy + "&order=" + order, ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertTrue(0 < Objects.requireNonNull(response.body()).size());
    var firstOrganization = (LinkedHashMap) Objects.requireNonNull(response.body()).get(0);
    assertEquals("The Washington Legal Clinic for the Homeless", firstOrganization.get("name"));
  }

  @Test
  void willRaiseExceptionForIllegalQueryParams() {
    var query = "nonsense";
    var queries = "nonsense,queries";
    var illegalQueryParamsException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/?query=" + query + "&queries=" + queries));
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, illegalQueryParamsException.getStatus());
    assertEquals("Illegal query parameters were entered.",
        illegalQueryParamsException.getMessage());

    var illegalPerPageParamsException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/?per_page=" + query));
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, illegalPerPageParamsException.getStatus());

    var illegalSortByParamsException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/?sort_by=" + query));
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, illegalSortByParamsException.getStatus());

    var illegalOrderParamsException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/?order=" + query));
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, illegalOrderParamsException.getStatus());
  }

  @Test
  void getOneOrgGivenId() throws JSONException {
    String organizationId = "C58FDCA7-8529-5B92-93CB-F80CE0D0330C";
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/" + organizationId, ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString =
        "{name:\"Anacostia Community Outreach Center (ACOC)\",id:\"" + organizationId + "\"}";
    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }

  @Test
  void cannotGetUnknownOrg() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + nonExistentOrg + "/", ArrayList.class));
    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }
}

package controller.organization.funding;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

@MicronautTest
class OrganizationFundingsControllerTests {

  private static String organizationOne = "7322ECBB-628C-20BF-91B9-F5AE907C69EF";
  private static String fundingOneOrganizationOne = "9ae632d8-1773-4af5-8b5d-3397afebbfbd";
  private static String nonExistentFunding = "DOESNOTEXIST";
  private static String organizationTwo = "1F69897C-657A-80EC-19C3-1D4BEAB52C3D";

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetIndexOfFundingForSpecificOrganization() {
    var responseStatus = client.toBlocking()
        .exchange("/organizations/" + organizationOne + "/fundings").status();
    assertEquals(HttpStatus.OK, responseStatus);
  }


  @Test
  void canGetSpecificFundingForSpecificOrganization() throws JSONException {
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/" + organizationOne + "/fundings/" + fundingOneOrganizationOne,
            ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString = "{organization_id: \"" + organizationOne
        + "\", source: \"generous volunteers\"}";

    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }


  @Test
  void cannotGetUnknownFundingOfSpecificOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/" + organizationOne + "/fundings/" + nonExistentFunding,
                ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());

  }

  @Test
  void cannotGetUnknownFundingOfUnknownOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
        .retrieve("/organizations/RANDOM-ORGANIZATION/fundings/NON-EXISTENT-SERVICE",
            ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void cannotGetSpecificFundingOfDifferentOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
        .retrieve("/organizations/" + organizationTwo + "/fundings/" + fundingOneOrganizationOne,
            ArrayList.class));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void getNotFoundExceptionForFundingOfEmptyOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .exchange("/organizations//fundings/"));

    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }
}

package controller.organization;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Objects;
import javax.inject.Inject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;

@MicronautTest
class OrganizationsFullControllerTests {

  private static String nonExistentOrg = "DOESNOTEXIST";

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetOrgFullIndexWithNoParams() {
    var response = client.toBlocking().exchange("/organizations/full/", ArrayList.class);
    assertEquals(HttpStatus.OK, response.getStatus());
    assertEquals(4, Objects.requireNonNull(response.body()).size());
  }

  @Test
  void canGetOrgFullWithQueryParam() {
    var query = "name";
    var queries = URLEncoder.encode("Department Of Human Services", StandardCharsets.UTF_8);
    var response = client.toBlocking()
        .exchange("/organizations/full?query=" + query + "&queries=" + queries, ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertEquals(1, Objects.requireNonNull(response.body()).size());
  }

  @Test
  void canGetOrgFullIndexWithPerPageParam() {
    var perPage = 2;
    var response = client.toBlocking()
        .exchange("/organizations/full?per_page=" + perPage, ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertEquals(2, Objects.requireNonNull(response.body()).size());
  }

  @Test
  void cannotGetOrgFullIndexWithMoreThan100PerPageParam() {
    var perPage = 1000;
    var response = client.toBlocking()
        .exchange("/organizations/full?per_page=" + perPage, ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertTrue(100 >= Objects.requireNonNull(response.body()).size());
  }

  @Test
  void canGetOrgFullIndexWithSortByParam() throws JSONException {
    var sortBy = "name";
    var response = client.toBlocking()
        .exchange("/organizations/full?sort_by=" + sortBy, ArrayList.class);

    assertEquals(HttpStatus.OK, response.status());
    var responseBody = Objects.requireNonNull(response.body());
    assertTrue(0 < responseBody.size());

    var jsonObject = new JSONObject((LinkedHashMap) responseBody.get(0));
    var expectedString =
        "{name:\"Anacostia Community Outreach Center (ACOC)\"}";
    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }

  @Test
  void canGetOrgFullIndexWithOrderParam() throws JSONException {
    var order = "desc";
    var sortBy = "name";
    var response = client.toBlocking()
        .exchange("/organizations/full?sort_by=" + sortBy + "&order=" + order, ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertEquals(HttpStatus.OK, response.status());
    var responseBody = Objects.requireNonNull(response.body());
    assertTrue(0 < responseBody.size());

    var jsonObject = new JSONObject((LinkedHashMap) responseBody.get(0));
    var expectedString =
        "{name:\"The Washington Legal Clinic for the Homeless\"}";
    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }

  @Test
  void willRaiseExceptionForIllegalQueryParams() {
    var query = "nonsense";
    var queries = "nonsense,queries";
    var illegalQueryParamsException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/full?query=" + query + "&queries=" + queries));
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, illegalQueryParamsException.getStatus());
    assertEquals("Illegal query parameters were entered.",
        illegalQueryParamsException.getMessage());

    var illegalPerPageParamsException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/full?per_page=" + query));
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, illegalPerPageParamsException.getStatus());

    var illegalSortByParamsException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/full?sort_by=" + query));
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, illegalSortByParamsException.getStatus());

    var illegalOrderParamsException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/full?order=" + query));
    assertEquals(HttpStatus.UNPROCESSABLE_ENTITY, illegalOrderParamsException.getStatus());
  }

  @Test
  void getOneOrgFullGivenId() throws JSONException {
    String organizationId = "C58FDCA7-8529-5B92-93CB-F80CE0D0330C";
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/full/" + organizationId, ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString =
        "{name:\"Anacostia Community Outreach Center (ACOC)\",id:\"" + organizationId + "\"}";
    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }

  @Test
  void cannotGetUnknownOrg() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/full/" + nonExistentOrg + "/", ArrayList.class));
    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }

  @Test
  void canGetOrganizationsFullIndexWithNoParams() {
    var response = client.toBlocking().exchange("/organizations/full/", ArrayList.class);
    assertEquals(HttpStatus.OK, response.status());
    assertEquals(4, Objects.requireNonNull(response.body()).size());
  }

  @Test
  void canGetOrganizationFull() throws JSONException {
    String organizationId = "C58FDCA7-8529-5B92-93CB-F80CE0D0330C";
    var responseArrayList = client.toBlocking()
        .retrieve("/organizations/full/" + organizationId, ArrayList.class);

    var jsonObject = new JSONObject((LinkedHashMap) responseArrayList.get(0));
    var expectedString =
        "{name:\"Anacostia Community Outreach Center (ACOC)\",id:\"" + organizationId + "\"}";
    JSONAssert.assertEquals(expectedString, jsonObject, false);
  }

  @Test
  void cannotGetUnknownOrganization() {
    var responseNotFoundException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/organizations/full/" + nonExistentOrg + "/", ArrayList.class));
    assertEquals(HttpStatus.NOT_FOUND, responseNotFoundException.getStatus());
  }
}

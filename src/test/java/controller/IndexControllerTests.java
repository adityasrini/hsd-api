package controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import io.micronaut.http.HttpStatus;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.micronaut.test.annotation.MicronautTest;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;

@MicronautTest
public class IndexControllerTests {

  @Inject
  @Client("/")
  RxHttpClient client;

  @Test
  void canGetIndex() {
    var responseStatus = client.toBlocking().exchange("/").status();
    assertEquals(HttpStatus.OK, responseStatus);
  }

  @Test
  void unauthorizedForUnlistedUrl() {
    var responseNotAuthorizedException = assertThrows(HttpClientResponseException.class,
        () -> client.toBlocking()
            .retrieve("/UNLISTED_URL"));
    assertEquals(HttpStatus.UNAUTHORIZED, responseNotAuthorizedException.getStatus());
  }
}

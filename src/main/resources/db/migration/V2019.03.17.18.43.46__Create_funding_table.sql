CREATE TABLE funding
(
  id              CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id CHAR(36) REFERENCES organization (id),
  service_id      CHAR(36) REFERENCES service (id),
  source          VARCHAR
);
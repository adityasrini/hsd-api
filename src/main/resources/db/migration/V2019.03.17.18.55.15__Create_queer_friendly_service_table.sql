CREATE TABLE queer_friendly_service
(
  id                     CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  service_at_location_id CHAR(36)             DEFAULT NULL REFERENCES service_at_location (id) ON DELETE CASCADE ON UPDATE CASCADE,
  queer_community        VARCHAR NOT NULL,
  details                TEXT                 DEFAULT NULL
);
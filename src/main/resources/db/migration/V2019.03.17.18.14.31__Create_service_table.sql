CREATE TABLE service
(
  id                      CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id         CHAR(36) NOT NULL REFERENCES organization (id) ON DELETE CASCADE ON UPDATE CASCADE,
  program_id              CHAR(36)             DEFAULT NULL REFERENCES program (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name                    VARCHAR  NOT NULL,
  alternate_name          VARCHAR              DEFAULT NULL,
  description             TEXT                 DEFAULT NULL,
  url                     VARCHAR              DEFAULT NULL,
  email                   VARCHAR              DEFAULT NULL,
  status                  VARCHAR  NOT NULL,
  interpretation_services VARCHAR              DEFAULT NULL,
  application_process     TEXT                 DEFAULT NULL,
  wait_time               VARCHAR              DEFAULT NULL,
  fees                    VARCHAR              DEFAULT NULL,
  accreditations          VARCHAR              DEFAULT NULL,
  licenses                VARCHAR              DEFAULT NULL
);
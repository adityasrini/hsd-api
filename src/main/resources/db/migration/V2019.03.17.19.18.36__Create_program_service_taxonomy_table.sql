CREATE TABLE program_service_taxonomy
(
  id              CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  program_id      CHAR(36) NOT NULL REFERENCES program (id) ON DELETE CASCADE ON UPDATE CASCADE,
  organization_id CHAR(36) NOT NULL REFERENCES organization (id) ON DELETE CASCADE ON UPDATE CASCADE,
  service_id      CHAR(36) NOT NULL REFERENCES service (id) ON DELETE CASCADE ON UPDATE CASCADE,
  taxonomy_id     CHAR(36) NOT NULL REFERENCES taxonomy (id) ON DELETE CASCADE ON UPDATE CASCADE,
  taxonomy_detail VARCHAR              DEFAULT NULL
);
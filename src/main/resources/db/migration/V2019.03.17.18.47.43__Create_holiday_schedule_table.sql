CREATE TABLE holiday_schedule
(
  id                     CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  service_at_location_id CHAR(36)             DEFAULT NULL REFERENCES service_at_location (id) ON DELETE CASCADE ON UPDATE CASCADE,
  closed                 BOOLEAN NOT NULL,
  opens_at               TIME WITH TIME ZONE,
  closes_at              TIME WITH TIME ZONE,
  start_date             DATE    NOT NULL,
  end_date               DATE    NOT NULL
);
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE organization
(
  id                CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  name              VARCHAR NOT NULL,
  alternate_name    VARCHAR              DEFAULT NULL,
  description       TEXT    NOT NULL,
  email             VARCHAR              DEFAULT NULL,
  url               VARCHAR              DEFAULT NULL,
  tax_status        VARCHAR              DEFAULT NULL,
  tax_id            VARCHAR              DEFAULT NULL,
  year_incorporated NUMERIC              DEFAULT NULL,
  legal_status      VARCHAR              DEFAULT NULL
);

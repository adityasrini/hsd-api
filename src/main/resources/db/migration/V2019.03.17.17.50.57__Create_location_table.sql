CREATE TABLE location
(
  id              CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id CHAR(36) NOT NULL REFERENCES organization (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR              DEFAULT NULL,
  alternate_name  VARCHAR              DEFAULT NULL,
  description     TEXT                 DEFAULT NULL,
  transportation  VARCHAR              DEFAULT NULL,
  latitude        NUMERIC              DEFAULT NULL,
  longitude       NUMERIC              DEFAULT NULL
);
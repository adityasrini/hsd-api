CREATE TABLE accessibility_for_disabilities
(
  id            CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  location_id   CHAR(36)             DEFAULT NULL REFERENCES location (id) ON DELETE CASCADE ON UPDATE CASCADE,
  accessibility VARCHAR              DEFAULT NULL,
  details       TEXT                 DEFAULT NULL
);
CREATE TABLE phone
(
  id                     CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id        CHAR(36)             DEFAULT NULL REFERENCES organization (id) ON DELETE CASCADE ON UPDATE CASCADE,
  contact_id             CHAR(36)             DEFAULT NULL REFERENCES contact (id) ON DELETE CASCADE ON UPDATE CASCADE,
  service_at_location_id CHAR(36)             DEFAULT NULL REFERENCES service_at_location (id) ON DELETE CASCADE ON UPDATE CASCADE,
  number                 VARCHAR NOT NULL,
  extension              INTEGER              DEFAULT NULL,
  type                   VARCHAR              DEFAULT NULL,
  language               VARCHAR              DEFAULT NULL,
  description            TEXT                 DEFAULT NULL
);
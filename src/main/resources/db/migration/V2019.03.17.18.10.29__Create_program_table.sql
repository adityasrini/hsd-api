CREATE TABLE program
(
  id              CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id CHAR(36) NOT NULL REFERENCES organization (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name            VARCHAR  NOT NULL,
  alternate_name  VARCHAR              DEFAULT NULL,
  description     TEXT                 DEFAULT NULL,
  url             VARCHAR              DEFAULT NULL
);
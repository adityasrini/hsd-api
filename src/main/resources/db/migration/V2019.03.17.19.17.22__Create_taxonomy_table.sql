CREATE TABLE taxonomy
(
  id          CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  name        VARCHAR NOT NULL UNIQUE,
  parent_id   character(36)        DEFAULT NULL REFERENCES taxonomy (id) ON DELETE CASCADE ON UPDATE CASCADE,
  parent_name VARCHAR              DEFAULT NULL REFERENCES taxonomy (name) ON DELETE CASCADE ON UPDATE CASCADE,
  vocabulary  VARCHAR              DEFAULT NULL
);
CREATE TABLE physical_address
(
  id             CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  location_id    CHAR(36)             DEFAULT NULL REFERENCES location (id) ON DELETE CASCADE ON UPDATE CASCADE,
  attention      VARCHAR              DEFAULT NULL,
  address_1      VARCHAR NOT NULL,
  city           VARCHAR NOT NULL,
  state_province VARCHAR NOT NULL,
  postal_code    VARCHAR NOT NULL,
  country        VARCHAR NOT NULL,
  region         VARCHAR              DEFAULT NULL
);
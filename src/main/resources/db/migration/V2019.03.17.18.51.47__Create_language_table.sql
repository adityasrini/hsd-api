CREATE TABLE language
(
  id          CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  service_id  CHAR(36)             DEFAULT NULL REFERENCES service (id) ON DELETE CASCADE ON UPDATE CASCADE,
  location_id CHAR(36)             DEFAULT NULL REFERENCES location (id),
  language    VARCHAR              DEFAULT NULL
);
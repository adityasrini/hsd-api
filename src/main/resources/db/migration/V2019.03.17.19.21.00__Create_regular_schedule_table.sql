CREATE TABLE regular_schedule
(
  id                     CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  service_at_location_id CHAR(36)             DEFAULT NULL REFERENCES service_at_location (id) ON DELETE CASCADE ON UPDATE CASCADE,
  weekday                INTEGER NOT NULL CHECK (weekday >= 1 AND weekday <= 7),
  opens_at               TIME WITH TIME ZONE,
  closes_at              TIME WITH TIME ZONE
);
--
-- PostgreSQL database dump
--

-- Dumped from database version 11.2
-- Dumped by pg_dump version 11.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: organization; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.organization (id, name, alternate_name, description, email, url, tax_status, tax_id, year_incorporated, legal_status) VALUES ('C58FDCA7-8529-5B92-93CB-F80CE0D0330C', 'Anacostia Community Outreach Center (ACOC)', 'ACOC', 'This is the Anacostia Community Outreach Center', NULL, 'http://www.anacostiaoutreach.org/wordpress/', NULL, NULL, NULL, NULL);
INSERT INTO public.organization (id, name, alternate_name, description, email, url, tax_status, tax_id, year_incorporated, legal_status) VALUES ('7322ECBB-628C-20BF-91B9-F5AE907C69EF', 'Casa Ruby', 'CR', 'This is Casa Ruby', NULL, 'http://www.casaruby.org', NULL, NULL, NULL, NULL);
INSERT INTO public.organization (id, name, alternate_name, description, email, url, tax_status, tax_id, year_incorporated, legal_status) VALUES ('1F69897C-657A-80EC-19C3-1D4BEAB52C3D', 'The Washington Legal Clinic for the Homeless', 'WLCH', 'This is the Washington Legal Clinic for the Homeless', NULL, 'http://www.legalclinic.org/', NULL, NULL, NULL, NULL);
INSERT INTO public.organization (id, name, alternate_name, description, email, url, tax_status, tax_id, year_incorporated, legal_status) VALUES ('44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Department of Human Services', 'DC DHS', 'This is Washington DC''s Department of Human Services', NULL, 'https://dcdhs.dc.gov', NULL, NULL, NULL, NULL);


--
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('C0BB401C-7F9D-28E3-5DB1-DE8F551A7F3C', 'C58FDCA7-8529-5B92-93CB-F80CE0D0330C', 'Food Pantry & Computer Center', NULL, 'http://www.anacostiaoutreach.org/wordpress/', NULL, 38.90022564, -76.97215679);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('B7A2372F-7826-36A5-9569-48FF3FB89AEB', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', 'Drop-Inn Safe Center', NULL, 'http://www.casaruby.org/drop.html', NULL, 38.92675810, -77.02315718);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Adams Place Drop-In Center', NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 38.91962229, -76.97487627);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('70EAD118-4FB0-12D0-94CB-7BC3FD8B459E', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Anacostia Service Center', NULL, 'https://dcdhs.dc.gov/node/117522', NULL, 38.86543825, -76.98923777);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('84B42F16-3092-7B94-6115-1FE849E15E14', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Congress Heights Service Center', NULL, 'https://dcdhs.dc.gov/node/117522', NULL, 38.82998701, -77.00814912);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('0F7B6248-184B-30EF-483E-FEE6C5B971BB', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Family Services Administration', NULL, 'https://dcdhs.dc.gov/service/homeless-and-homeless-prevention-services', NULL, 38.90917422, -77.00696358);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('4FE91528-95FE-706A-6B5A-05B804E1349B', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Fort Davis Service Center', NULL, 'https://dcdhs.dc.gov/node/117522', NULL, 38.86612122, -76.94994864);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('0B107868-996B-6160-3EDA-32570BC139B2', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'H Street Service Center', NULL, 'https://dcdhs.dc.gov/node/117522', NULL, 38.89997477, -76.99645358);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('C375C828-647D-948D-03CC-8C9778F76D78', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Taylor Street Service Center', NULL, 'https://dcdhs.dc.gov/node/117522', NULL, 38.94110672, -77.02724783);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('FAEE16BB-5079-0490-2430-BCA1AE241869', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Virginia Williams Family Resource Center', NULL, 'https://dcdhs.dc.gov/page/virginia-williams-family-resource-center', NULL, 38.92256186, -76.99305218);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('47C7062B-4F3D-9234-1D34-1CDFA4576A57', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Hotline', NULL, NULL, NULL, 38.90917400, -77.00696400);
INSERT INTO public.location (id, organization_id, name, alternate_name, description, transportation, latitude, longitude) VALUES ('69AAE437-3881-6A60-6EA9-FBBF32871974', '1F69897C-657A-80EC-19C3-1D4BEAB52C3D', NULL, NULL, NULL, NULL, 38.91673953, -77.02833038);


--
-- Data for Name: accessibility_for_disabilities; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.accessibility_for_disabilities (id, location_id, accessibility, details) VALUES ('25931306-0F79-4431-8B14-9D25505607D7', '0F7B6248-184B-30EF-483E-FEE6C5B971BB', 'true', NULL);
INSERT INTO public.accessibility_for_disabilities (id, location_id, accessibility, details) VALUES ('25E09F68-0CB4-426A-9F58-C423583297DB', '47C7062B-4F3D-9234-1D34-1CDFA4576A57', 'true', NULL);


--
-- Data for Name: program; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.program (id, organization_id, name, alternate_name, description, url) VALUES ('5A35D6EA-53AF-8F29-5DA9-7D17E67B74C7', '1F69897C-657A-80EC-19C3-1D4BEAB52C3D', 'Legal Assistance Project', NULL, NULL, 'http://www.legalclinic.org/');
INSERT INTO public.program (id, organization_id, name, alternate_name, description, url) VALUES ('93AB3E78-4DF5-7242-5487-249126436F31', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Shelter Hotline', NULL, NULL, 'https://dhs.dc.gov/service/hypo-hyperthermia-watch');
INSERT INTO public.program (id, organization_id, name, alternate_name, description, url) VALUES ('93AB3E78-4DF5-7242-5487-249126436F32', '44EEB4B4-48C6-9BD6-9429-454ED533353B', 'Another Shelter Hotline', NULL, NULL, 'https://dhs.dc.gov/service/hypo-hyperthermia-watch');


--
-- Data for Name: service; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('215691A1-998C-5BB9-3BC3-163A47292996', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, 'Shelter', NULL, NULL, 'http://www.casaruby.org/index.html', NULL, 'STATUS UNKNOWN', NULL, 'Public OR Call Shelter', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('C6B9E941-573F-9365-93D1-BF6ED3D69856', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'ASSESSMENT', NULL, NULL, 'https://dcdhs.dc.gov/service/homeless-and-homeless-prevention-services', NULL, 'STATUS UNKNOWN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('A3D21E44-6239-30F3-4C9A-4D836D6F1BD6', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, 'COMPUTERS', NULL, NULL, 'http://www.casaruby.org/drop.html', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('511DEDAD-A15D-6B36-0434-97F56E176254', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'DOMESTIC_VIOLENCE_SERVICES', NULL, NULL, 'https://dcdhs.dc.gov/page/virginia-williams-family-resource-center', NULL, 'STATUS UNKNOWN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('A63FF3EA-0FF8-36D8-13EB-9360DAAB7600', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'HOUSING', NULL, NULL, 'https://dcdhs.dc.gov/service/homeless-and-homeless-prevention-services', NULL, 'STATUS UNKNOWN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('E9113180-6CEB-3D77-5673-6C460A9979A6', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'LAUNDRY', NULL, NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('54112EBC-7C6C-000C-2DA0-05B07D5FA0CA', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'SHOWERS', NULL, NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('E35860D6-61F5-8DAE-98BA-2A35E3483713', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'SNAP_FOOD_STAMPS', NULL, NULL, 'https://dcdhs.dc.gov/node/117522', NULL, 'STATUS UNKNOWN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('E2FE3ACD-28FF-0651-0228-5E96734D13F9', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'TANF_FINANCIAL_ASSISTANCE', NULL, NULL, 'https://dcdhs.dc.gov/node/117522', NULL, 'STATUS UNKNOWN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('F2234502-6657-3778-9CFF-4FE911990783', '44EEB4B4-48C6-9BD6-9429-454ED533353B', '93AB3E78-4DF5-7242-5487-249126436F31', 'TRANSPORTATION', NULL, NULL, 'https://dhs.dc.gov/service/hypo-hyperthermia-watch', NULL, 'STATUS UNKNOWN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('1F293AA9-15E8-5B1B-5EF0-1F0B5B153E5D', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, 'CLOTHING', NULL, NULL, 'http://www.casaruby.org/drop.html', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('A747695C-2A12-2234-2A96-5408A1027991', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'HOUSING_NAVIGATION', NULL, NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('F8E7AC4F-949E-4143-8C0F-52C44B752FDC', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'HAIRCUTS', NULL, NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('D4AFC11B-90DF-4F10-44FE-B9D8930E813B', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, 'CASE_MANAGEMENT', NULL, NULL, 'http://www.casaruby.org/drop.html', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('BE2CE8DD-0CAF-80E5-8558-4BD035E90AE5', '1F69897C-657A-80EC-19C3-1D4BEAB52C3D', '5A35D6EA-53AF-8F29-5DA9-7D17E67B74C7', 'LEGAL_SERVICES', NULL, NULL, 'http://www.legalclinic.org/', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('7951EE5D-9437-3B52-7C08-D6AC01BB2383', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'VOCATIONAL_TRAINING', NULL, NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('CDDAE1BE-45F1-4B08-5A2A-73FB6B3F8174', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'COMPUTERS', NULL, NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('DE7FA814-1F32-5361-7CF9-24BC8EF67F17', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'CLOTHING', NULL, NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('28DC1A46-8B74-28BB-828E-8EE7E5DA5241', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, 'ASSESSMENT', NULL, NULL, 'http://www.casaruby.org/drop.html', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('C7FBBA2A-3F23-4039-4E04-5EA273E0193E', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, 'GROUPS', NULL, NULL, 'http://www.casaruby.org/drop.html', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('DB345558-9BFF-A1A8-1F1C-CF8910FB2BB7', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, 'MEALS', NULL, NULL, 'http://www.casaruby.org/drop.html', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('AD100D57-933F-965E-7A8B-6DCCAB539357', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, 'MEALS', NULL, NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('AA9F8B20-5853-4BCE-5FF7-4627F7E07968', 'C58FDCA7-8529-5B92-93CB-F80CE0D0330C', NULL, 'COMPUTERS', NULL, NULL, 'http://www.anacostiaoutreach.org/wordpress/', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('98AEDC3F-006B-8023-757F-EDB012EE4947', 'C58FDCA7-8529-5B92-93CB-F80CE0D0330C', NULL, 'FOOD_GROCERIES', NULL, NULL, 'http://www.anacostiaoutreach.org/wordpress/', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('D5DE3453-65B0-8154-3196-56E58CB7795C', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, 'HOUSING_NAVIGATION', NULL, NULL, 'http://www.casaruby.org/drop.html', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('C2A5B279-9DEF-322B-7266-954CDF2AA711', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, 'LEGAL_SERVICES', NULL, NULL, 'http://www.casaruby.org/drop.html', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);
INSERT INTO public.service (id, organization_id, program_id, name, alternate_name, description, url, email, status, interpretation_services, application_process, wait_time, fees, accreditations, licenses) VALUES ('003E6F02-7B22-7C0B-25F8-CA288D9D5370', '44EEB4B4-48C6-9BD6-9429-454ED533353B', '93AB3E78-4DF5-7242-5487-249126436F32', 'CASE_MANAGEMENT', NULL, NULL, 'https://dcdhs.dc.gov/page/drop-center', NULL, 'STATUS UNKNOWN', NULL, 'Public', NULL, NULL, NULL, NULL);


--
-- Data for Name: age_eligibility; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('77bdd9ae-08b0-421b-bb90-85f236347428', 'AA9F8B20-5853-4BCE-5FF7-4627F7E07968', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('fda28058-b710-404d-9b20-be11df055ff7', '98AEDC3F-006B-8023-757F-EDB012EE4947', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('794a1a14-b1c4-492d-8d22-9263a3215042', '511DEDAD-A15D-6B36-0434-97F56E176254', 'At least one dependent <18 years old');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('eb9bf4f2-99e1-41a0-ab10-1a51fcfc7b1c', 'E9113180-6CEB-3D77-5673-6C460A9979A6', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('8f50aa46-e75d-41e2-b402-10fd720b53e6', '54112EBC-7C6C-000C-2DA0-05B07D5FA0CA', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('aed459ed-08c9-4e49-8516-d77a32526954', '7951EE5D-9437-3B52-7C08-D6AC01BB2383', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('c493bfbe-3f91-4df1-bd4a-897d0e27bd38', 'CDDAE1BE-45F1-4B08-5A2A-73FB6B3F8174', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('1bbd1865-fa14-4138-ad42-71dc07fa15c2', 'DE7FA814-1F32-5361-7CF9-24BC8EF67F17', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('45da3825-0d0f-41e6-ae3b-2961106b8132', '003E6F02-7B22-7C0B-25F8-CA288D9D5370', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('0cef5447-da5c-4aea-a793-9117be866345', 'F8E7AC4F-949E-4143-8C0F-52C44B752FDC', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('f1e3df8f-1216-4fde-96a2-6cc9e03481f6', 'A747695C-2A12-2234-2A96-5408A1027991', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('7b2c8ce5-8db9-4016-a3f1-6874e6ceb52a', 'AD100D57-933F-965E-7A8B-6DCCAB539357', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('0097968c-4920-46e8-be54-5f7a3df040e9', 'E2FE3ACD-28FF-0651-0228-5E96734D13F9', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('4b503c1c-e71d-4e51-a730-9fb5e9267a9a', 'A63FF3EA-0FF8-36D8-13EB-9360DAAB7600', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('0a4abb77-6fb5-4003-be5f-1279a476f846', 'C6B9E941-573F-9365-93D1-BF6ED3D69856', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('160be77b-ab2e-42c9-a729-8f229956663c', 'E35860D6-61F5-8DAE-98BA-2A35E3483713', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('b0f25cb5-d9e2-47a8-9c14-44e8c436466a', 'F2234502-6657-3778-9CFF-4FE911990783', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('c4185c4d-ca7d-44ce-91b6-26e166ef1208', 'BE2CE8DD-0CAF-80E5-8558-4BD035E90AE5', '18+');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('891211e8-5349-42bb-8640-4a6f55eece47', '1F293AA9-15E8-5B1B-5EF0-1F0B5B153E5D', '18-24');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('c2a34597-4522-47c8-85f9-5204a9d8e386', 'A3D21E44-6239-30F3-4C9A-4D836D6F1BD6', '18-24');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('260439c5-31bc-427d-b6b3-3fd85680f744', 'C7FBBA2A-3F23-4039-4E04-5EA273E0193E', '18-24');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('90ecfc94-5dbc-49d5-9c19-cdd73481cd4f', 'C2A5B279-9DEF-322B-7266-954CDF2AA711', '18-24');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('547ff413-befc-40d2-9c3d-6e947a341097', 'DB345558-9BFF-A1A8-1F1C-CF8910FB2BB7', '18-24');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('aef731b4-04b1-4c89-801d-2785d0318b80', '215691A1-998C-5BB9-3BC3-163A47292996', '18-24');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('1b1139ec-093b-4aa3-8074-f0b3f7e6589c', '28DC1A46-8B74-28BB-828E-8EE7E5DA5241', '18-24');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('034e211b-1e23-4725-93ae-618db533149f', 'D4AFC11B-90DF-4F10-44FE-B9D8930E813B', '18-24');
INSERT INTO public.age_eligibility (id, service_id, age_eligibility) VALUES ('492d9054-9aa2-4247-828d-03983f19ddfc', 'D5DE3453-65B0-8154-3196-56E58CB7795C', '18-24');


--
-- Data for Name: service_at_location; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('5B3771B4-4EAC-948F-6F1C-55BA905B1074', 'AA9F8B20-5853-4BCE-5FF7-4627F7E07968', 'C0BB401C-7F9D-28E3-5DB1-DE8F551A7F3C', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('AC42AC1C-A59B-1684-0925-84B1E2998740', '98AEDC3F-006B-8023-757F-EDB012EE4947', 'C0BB401C-7F9D-28E3-5DB1-DE8F551A7F3C', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('05267B59-A1D5-737C-1CFA-157EAC8C341F', '28DC1A46-8B74-28BB-828E-8EE7E5DA5241', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('3C1A2D6F-79B9-2C14-6E97-E6A2E5476897', 'D4AFC11B-90DF-4F10-44FE-B9D8930E813B', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('659C5900-66BC-7010-4438-6494969A1D37', '1F293AA9-15E8-5B1B-5EF0-1F0B5B153E5D', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('FDA3B4A5-2A11-14CE-0952-BCDDCC3F3FFF', 'A3D21E44-6239-30F3-4C9A-4D836D6F1BD6', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('922DD7B2-1D10-7624-747F-7D3D58EB533F', 'C7FBBA2A-3F23-4039-4E04-5EA273E0193E', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('EB70825F-73F3-1BC3-484D-C21FDDBE312B', 'D5DE3453-65B0-8154-3196-56E58CB7795C', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('10C4253B-39EA-7385-5B9C-3702E5E15B17', 'C2A5B279-9DEF-322B-7266-954CDF2AA711', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('64488FAD-9AC1-938E-5B19-B81A9D719CA6', 'DB345558-9BFF-A1A8-1F1C-CF8910FB2BB7', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('262E86F8-0244-348B-420A-70BFA7AD24A4', '215691A1-998C-5BB9-3BC3-163A47292996', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('45ADF025-83B3-4AEA-0170-5675DE686369', '003E6F02-7B22-7C0B-25F8-CA288D9D5370', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('807D90C8-96BD-5115-343A-A08324249F87', 'DE7FA814-1F32-5361-7CF9-24BC8EF67F17', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('055BD96D-34EC-1828-1274-C78A61007003', 'CDDAE1BE-45F1-4B08-5A2A-73FB6B3F8174', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('87806802-3AA4-0881-3E54-5102482A8184', 'F8E7AC4F-949E-4143-8C0F-52C44B752FDC', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('3FADFDAC-6BDA-2F42-0A70-5E4715915049', 'A747695C-2A12-2234-2A96-5408A1027991', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('F58F5602-869D-7A54-4584-F867546F4ECF', 'E9113180-6CEB-3D77-5673-6C460A9979A6', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('E059E21D-7EAF-8174-7756-79A53BF12887', 'AD100D57-933F-965E-7A8B-6DCCAB539357', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('6A40301F-7044-1B65-7DDC-BCE264843DEB', '54112EBC-7C6C-000C-2DA0-05B07D5FA0CA', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('130AA05C-7A42-9069-6C2D-0506BD3A9D13', '7951EE5D-9437-3B52-7C08-D6AC01BB2383', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('723165C8-7FFF-2931-8464-93D91A9B4B9D', 'E35860D6-61F5-8DAE-98BA-2A35E3483713', '70EAD118-4FB0-12D0-94CB-7BC3FD8B459E', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('F021D809-0996-9BDC-3A8D-0C5908B6A791', 'E2FE3ACD-28FF-0651-0228-5E96734D13F9', '70EAD118-4FB0-12D0-94CB-7BC3FD8B459E', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('3C24F9BB-6356-929B-72B6-41ED148F11CA', 'E35860D6-61F5-8DAE-98BA-2A35E3483713', '84B42F16-3092-7B94-6115-1FE849E15E14', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('128ABEA9-A150-7BD7-A4D6-CFEEF38F0272', 'E2FE3ACD-28FF-0651-0228-5E96734D13F9', '84B42F16-3092-7B94-6115-1FE849E15E14', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('945352F4-7DDD-44FD-5BC9-BB29B9F49028', 'C6B9E941-573F-9365-93D1-BF6ED3D69856', '0F7B6248-184B-30EF-483E-FEE6C5B971BB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('47EE49F9-3412-0C2F-4E65-5D7CA5FD3E8B', 'A63FF3EA-0FF8-36D8-13EB-9360DAAB7600', '0F7B6248-184B-30EF-483E-FEE6C5B971BB', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('D76DFCDF-3C03-900E-0F9B-33DFE74D744A', 'E35860D6-61F5-8DAE-98BA-2A35E3483713', '4FE91528-95FE-706A-6B5A-05B804E1349B', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('F24E9CD7-3438-34EB-9FAB-BFDE04C83AAC', 'E2FE3ACD-28FF-0651-0228-5E96734D13F9', '4FE91528-95FE-706A-6B5A-05B804E1349B', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('3FF977B2-90EA-5F03-2ADC-428E6F913424', 'E35860D6-61F5-8DAE-98BA-2A35E3483713', '0B107868-996B-6160-3EDA-32570BC139B2', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('6B9B2F4A-8F2B-1469-455C-168FD9DB4E78', 'E2FE3ACD-28FF-0651-0228-5E96734D13F9', '0B107868-996B-6160-3EDA-32570BC139B2', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('9BA22DE7-702A-0B40-79AA-EDB2564153AF', 'E35860D6-61F5-8DAE-98BA-2A35E3483713', 'C375C828-647D-948D-03CC-8C9778F76D78', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('9DE40CDD-3B21-00A0-3840-2813B54F42D7', 'E2FE3ACD-28FF-0651-0228-5E96734D13F9', 'C375C828-647D-948D-03CC-8C9778F76D78', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('6288CFD6-9FE9-77E0-4844-9698990E5161', '511DEDAD-A15D-6B36-0434-97F56E176254', 'FAEE16BB-5079-0490-2430-BCA1AE241869', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('3710C758-A37D-6FFA-5F50-B9AEF5B73E92', 'F2234502-6657-3778-9CFF-4FE911990783', '47C7062B-4F3D-9234-1D34-1CDFA4576A57', NULL);
INSERT INTO public.service_at_location (id, service_id, location_id, description) VALUES ('483E7B7A-9525-2FC3-111A-EF7C199B0FAF', 'BE2CE8DD-0CAF-80E5-8558-4BD035E90AE5', '69AAE437-3881-6A60-6EA9-FBBF32871974', NULL);


--
-- Data for Name: contact; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.contact (id, organization_id, service_at_location_id, name, title, department, email) VALUES ('4F515802-906E-495F-94A6-9767F3CEB6F7', 'C58FDCA7-8529-5B92-93CB-F80CE0D0330C', '5B3771B4-4EAC-948F-6F1C-55BA905B1074', 'Person', 'Chief Person', 'Some Department', 'no-reply@blah.com');
INSERT INTO public.contact (id, organization_id, service_at_location_id, name, title, department, email) VALUES ('4F515802-906E-495F-94A6-9767F3CEB6F8', '1F69897C-657A-80EC-19C3-1D4BEAB52C3D', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', 'Person Two', 'Subordinate Person', 'Some Other Department', 'no-reply-two@blah.com');


--
-- Data for Name: flyway_schema_history; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: funding; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.funding (id, organization_id, service_id, source) VALUES ('a6a3168e-c972-4152-a744-5484e0b064a2', '1F69897C-657A-80EC-19C3-1D4BEAB52C3D', 'BE2CE8DD-0CAF-80E5-8558-4BD035E90AE5', 'pro bono');
INSERT INTO public.funding (id, organization_id, service_id, source) VALUES ('9ae632d8-1773-4af5-8b5d-3397afebbfbd', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', '215691A1-998C-5BB9-3BC3-163A47292996', 'generous volunteers');


--
-- Data for Name: gender_eligibility; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('3bbd85a5-9305-46ab-8b32-dbf395698040', '28DC1A46-8B74-28BB-828E-8EE7E5DA5241', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('7faf0e2d-f4ec-4f77-9a36-b1d210bf3250', 'CDDAE1BE-45F1-4B08-5A2A-73FB6B3F8174', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('0303f61d-568d-46eb-b433-1c1a6dc914ff', 'E2FE3ACD-28FF-0651-0228-5E96734D13F9', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('d85fc3db-9528-408e-9ee3-6feed4412b5f', '7951EE5D-9437-3B52-7C08-D6AC01BB2383', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('ae42df42-20c1-428c-9098-dd7afcb34237', '215691A1-998C-5BB9-3BC3-163A47292996', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('703f0ea5-9b35-4dfe-982c-803ec2ceb13c', 'D4AFC11B-90DF-4F10-44FE-B9D8930E813B', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('1362b56b-bf57-4111-b130-4b25fe93e007', 'A747695C-2A12-2234-2A96-5408A1027991', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('15b0667a-cc14-4db2-ac8a-9f957699a64f', 'E35860D6-61F5-8DAE-98BA-2A35E3483713', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('f5d24097-9340-4762-8865-7dd57b436ec9', '003E6F02-7B22-7C0B-25F8-CA288D9D5370', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('921fc78c-772f-45f6-9342-175680c31abb', 'A63FF3EA-0FF8-36D8-13EB-9360DAAB7600', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('303eeabb-4ac2-458a-b028-668c00d4ad15', 'AA9F8B20-5853-4BCE-5FF7-4627F7E07968', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('d85f9808-7f42-4bf3-bb54-da8990bfaf3a', '511DEDAD-A15D-6B36-0434-97F56E176254', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('12b5f302-f1b5-4f3c-a1d3-af5275d08991', 'DB345558-9BFF-A1A8-1F1C-CF8910FB2BB7', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('d7b4f28e-2fd6-4d0e-a242-d2115d6e369c', '54112EBC-7C6C-000C-2DA0-05B07D5FA0CA', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('e8e321aa-3a88-4187-8f6a-137b71d66c0f', 'DE7FA814-1F32-5361-7CF9-24BC8EF67F17', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('0bb868f8-120e-4a18-93c4-a0907acd9143', 'F8E7AC4F-949E-4143-8C0F-52C44B752FDC', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('a01bf5cd-7815-4f2a-8257-501fe9255edf', 'C6B9E941-573F-9365-93D1-BF6ED3D69856', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('b4cc0f02-ddb5-497e-9cc4-77ea44adef52', '98AEDC3F-006B-8023-757F-EDB012EE4947', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('e18936d3-4492-4bef-92d6-22639e4c904f', 'AD100D57-933F-965E-7A8B-6DCCAB539357', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('836ef8a5-c809-42ce-a489-470335e59826', 'E9113180-6CEB-3D77-5673-6C460A9979A6', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('99289ec2-f74a-4725-a90d-b8d7b36561a5', 'A3D21E44-6239-30F3-4C9A-4D836D6F1BD6', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('04d309e9-3a82-4ff5-8e59-f834c4214a28', 'BE2CE8DD-0CAF-80E5-8558-4BD035E90AE5', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('805d0fd0-e2a5-4ad1-ad3d-57b42116e45e', 'F2234502-6657-3778-9CFF-4FE911990783', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('70001c6c-0fbe-45f4-8077-4a67e870efad', '1F293AA9-15E8-5B1B-5EF0-1F0B5B153E5D', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('866765ba-47b6-466c-8555-41a44657a9a0', 'C7FBBA2A-3F23-4039-4E04-5EA273E0193E', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('a3a4fad1-bfb9-4a61-89cc-150fd388ded5', 'D5DE3453-65B0-8154-3196-56E58CB7795C', 'Mixed');
INSERT INTO public.gender_eligibility (id, service_id, gender_eligibility) VALUES ('258e0f3b-8749-467a-9bf9-74873cd8eecc', 'C2A5B279-9DEF-322B-7266-954CDF2AA711', 'Mixed');


--
-- Data for Name: holiday_schedule; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.holiday_schedule (id, service_at_location_id, closed, opens_at, closes_at, start_date, end_date) VALUES ('0C22FD07-2197-4FBA-874B-9E80EFD3B9B3', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', false, '00:00:00-05', '23:59:00-05', '1970-01-01', '2100-01-01');
INSERT INTO public.holiday_schedule (id, service_at_location_id, closed, opens_at, closes_at, start_date, end_date) VALUES ('DE389672-FAB2-4190-83C3-C4165F5BC5DC', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', false, '00:00:00-05', '23:59:00-05', '1970-01-01', '2100-01-01');
INSERT INTO public.holiday_schedule (id, service_at_location_id, closed, opens_at, closes_at, start_date, end_date) VALUES ('D61CD567-AFFE-4947-87AA-08120197FF83', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', false, '00:00:00-05', '23:59:00-05', '1970-01-01', '2100-01-01');
INSERT INTO public.holiday_schedule (id, service_at_location_id, closed, opens_at, closes_at, start_date, end_date) VALUES ('EA27AEDC-F6DE-49BF-AD0B-6168A194555E', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', false, '00:00:00-05', '23:59:00-05', '1970-01-01', '2100-01-01');
INSERT INTO public.holiday_schedule (id, service_at_location_id, closed, opens_at, closes_at, start_date, end_date) VALUES ('FBB3DA5A-7853-496B-BBBA-B3493BC38627', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', false, '00:00:00-05', '23:59:00-05', '1970-01-01', '2100-01-01');
INSERT INTO public.holiday_schedule (id, service_at_location_id, closed, opens_at, closes_at, start_date, end_date) VALUES ('E397CA74-D8EE-40FF-B1AD-39142808B13F', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', false, '00:00:00-05', '23:59:00-05', '1970-01-01', '2100-01-01');
INSERT INTO public.holiday_schedule (id, service_at_location_id, closed, opens_at, closes_at, start_date, end_date) VALUES ('60FBE3E9-44A0-4843-ADE5-350E7F050272', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', false, '00:00:00-05', '23:59:00-05', '1970-01-01', '2100-01-01');


--
-- Data for Name: language; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: queer_friendly_service; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.queer_friendly_service (id, service_at_location_id, queer_community, details) VALUES ('8A9B45B4-99F7-4F1C-B164-4C91047BE7E8', '05267B59-A1D5-737C-1CFA-157EAC8C341F', 'true', NULL);
INSERT INTO public.queer_friendly_service (id, service_at_location_id, queer_community, details) VALUES ('AC5DAA53-3687-44BC-A6C5-06060C324597', '3C1A2D6F-79B9-2C14-6E97-E6A2E5476897', 'true', NULL);
INSERT INTO public.queer_friendly_service (id, service_at_location_id, queer_community, details) VALUES ('0F763B36-A5E4-4DCF-B4E1-C6A0FA101BCB', '659C5900-66BC-7010-4438-6494969A1D37', 'true', NULL);
INSERT INTO public.queer_friendly_service (id, service_at_location_id, queer_community, details) VALUES ('C14CE291-3BC3-4B61-9691-7C06285CEBD9', 'FDA3B4A5-2A11-14CE-0952-BCDDCC3F3FFF', 'true', NULL);
INSERT INTO public.queer_friendly_service (id, service_at_location_id, queer_community, details) VALUES ('AA1EDF04-AD8B-4E56-901C-EB6071B184D8', '922DD7B2-1D10-7624-747F-7D3D58EB533F', 'true', NULL);
INSERT INTO public.queer_friendly_service (id, service_at_location_id, queer_community, details) VALUES ('52F2F48F-1996-43DB-AB8C-0C60192D314C', 'EB70825F-73F3-1BC3-484D-C21FDDBE312B', 'true', NULL);
INSERT INTO public.queer_friendly_service (id, service_at_location_id, queer_community, details) VALUES ('28024561-4E87-424D-829B-765E6303FC14', '10C4253B-39EA-7385-5B9C-3702E5E15B17', 'true', NULL);
INSERT INTO public.queer_friendly_service (id, service_at_location_id, queer_community, details) VALUES ('749ECB31-3FE2-49AD-A782-80959B542796', '64488FAD-9AC1-938E-5B19-B81A9D719CA6', 'true', NULL);
INSERT INTO public.queer_friendly_service (id, service_at_location_id, queer_community, details) VALUES ('89FD30A3-42A3-43CC-A578-4FC1CA2CCD84', '262E86F8-0244-348B-420A-70BFA7AD24A4', 'true', NULL);


--
-- Data for Name: phone; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.phone (id, organization_id, contact_id, service_at_location_id, number, extension, type, language, description) VALUES ('0CD1C3A0-8CC9-1413-5F03-897C30B22E6B', '44EEB4B4-48C6-9BD6-9429-454ED533353B', NULL, '3710C758-A37D-6FFA-5F50-B9AEF5B73E92', '202-399-7093', NULL, NULL, NULL, 'shelter hotline');
INSERT INTO public.phone (id, organization_id, contact_id, service_at_location_id, number, extension, type, language, description) VALUES ('2DE2CA28-8A86-03E1-0887-06E940044776', '1F69897C-657A-80EC-19C3-1D4BEAB52C3D', NULL, '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', '202-328-5500', NULL, NULL, NULL, NULL);
INSERT INTO public.phone (id, organization_id, contact_id, service_at_location_id, number, extension, type, language, description) VALUES ('901CEFB5-3C7F-6454-0A83-8D4A54FC10F2', '7322ECBB-628C-20BF-91B9-F5AE907C69EF', NULL, '262E86F8-0244-348B-420A-70BFA7AD24A4', '311', NULL, NULL, NULL, 'shelter hotline');


--
-- Data for Name: physical_address; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('333E3A2F-0A36-2EEE-766B-1A4A484822AE', 'C0BB401C-7F9D-28E3-5DB1-DE8F551A7F3C', NULL, '711 24TH STREET NE', 'Washington ', 'DC', '20002', 'US', 'Ward 5');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('4C30CFAB-0084-0EB7-3217-CDF51EA35A55', 'B7A2372F-7826-36A5-9569-48FF3FB89AEB', NULL, '2822 GEORGIA AVENUE NW', 'Washington ', 'DC', '20001', 'US', 'Ward 1');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('1A31631D-5555-8376-00FB-8E8509D034ED', '1F5BF9BC-0AB0-442F-4CC4-57AEAE40319D', NULL, '2210 ADAMS PLACE NE', 'Washington ', 'DC', '20018', 'US', 'Ward 5');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('EFC25925-7759-7A49-34B4-AD7C25C94661', '70EAD118-4FB0-12D0-94CB-7BC3FD8B459E', NULL, '2100 MARTIN LUTHER KING JR AVENUE SE', 'Washington ', 'DC', '20020', 'US', 'Ward 8');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('EB33729D-4F50-2864-4C2F-B662AAF618E2', '84B42F16-3092-7B94-6115-1FE849E15E14', NULL, '4049 SOUTH CAPITOL STREET SW', 'Washington ', 'DC', '20032', 'US', 'Ward 8');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('6BC4CC4F-16DD-55FA-5C27-7574E8688233', '0F7B6248-184B-30EF-483E-FEE6C5B971BB', NULL, '64 NEW YORK AVENUE NE', 'Washington ', 'DC', '20002', 'US', 'Ward 5');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('B0B844A6-4C85-27A6-14C9-9B89B6EE5E93', '4FE91528-95FE-706A-6B5A-05B804E1349B', NULL, '3851 ALABAMA AVENUE SE', 'Washington ', 'DC', '20020', 'US', 'Ward 7');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('6ED73452-5079-6457-38E9-E40A797F173D', '0B107868-996B-6160-3EDA-32570BC139B2', NULL, '645 H STREET NE', 'Washington ', 'DC', '20002', 'US', 'Ward 6');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('6B6DD145-359C-7026-1736-66E1CBAF9C35', 'C375C828-647D-948D-03CC-8C9778F76D78', NULL, '1207 TAYLOR STREET NW', 'Washington ', 'DC', '20011', 'US', 'Ward 4');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('88DE5681-6C8C-8C84-1F7A-94CAFD024573', 'FAEE16BB-5079-0490-2430-BCA1AE241869', NULL, '920 RHODE ISLAND AVENUE NE', 'Washington ', 'DC', '20018', 'US', 'Ward 5');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('C52FF499-7413-2BAC-7B43-44D9B76833A4', '47C7062B-4F3D-9234-1D34-1CDFA4576A57', NULL, '64 NEW YORK AVENUE NE', 'Washington ', 'DC', '20002', 'US', 'Ward 5');
INSERT INTO public.physical_address (id, location_id, attention, address_1, city, state_province, postal_code, country, region) VALUES ('2CA84C45-9B7A-9111-0371-B9F057F447EC', '69AAE437-3881-6A60-6EA9-FBBF32871974', NULL, '1200 U STREET NW', 'Washington ', 'DC', '20009', 'US', 'Ward 1');


--
-- Data for Name: taxonomy; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: program_service_taxonomy; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- Data for Name: regular_schedule; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.regular_schedule (id, service_at_location_id, weekday, opens_at, closes_at) VALUES ('AE9E6C52-6B56-25B8-96FA-C0C2B538331F', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', 1, '00:00:00-05', '23:59:00-05');
INSERT INTO public.regular_schedule (id, service_at_location_id, weekday, opens_at, closes_at) VALUES ('1BDCEB6A-96A9-01CC-5413-2E79E6529DA0', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', 2, '00:00:00-05', '23:59:00-05');
INSERT INTO public.regular_schedule (id, service_at_location_id, weekday, opens_at, closes_at) VALUES ('9AF4AE86-2F91-6458-33CF-994879651F18', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', 3, '00:00:00-05', '23:59:00-05');
INSERT INTO public.regular_schedule (id, service_at_location_id, weekday, opens_at, closes_at) VALUES ('BCC8D918-75DC-88EC-06A3-12347AD50C20', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', 4, '00:00:00-05', '23:59:00-05');
INSERT INTO public.regular_schedule (id, service_at_location_id, weekday, opens_at, closes_at) VALUES ('639EC140-6351-86D5-7E7F-F623FB6D089A', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', 5, '00:00:00-05', '23:59:00-05');
INSERT INTO public.regular_schedule (id, service_at_location_id, weekday, opens_at, closes_at) VALUES ('A200B49B-6B28-2699-9E4F-27CE69243173', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', 6, '00:00:00-05', '23:59:00-05');
INSERT INTO public.regular_schedule (id, service_at_location_id, weekday, opens_at, closes_at) VALUES ('3DD620C1-61F5-3817-6D18-5FCB30BA7150', '483E7B7A-9525-2FC3-111A-EF7C199B0FAF', 7, '00:00:00-05', '23:59:00-05');


--
-- Data for Name: service_area; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- PostgreSQL database dump complete
--


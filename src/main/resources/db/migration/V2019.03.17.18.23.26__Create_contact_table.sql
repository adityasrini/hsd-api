CREATE TABLE contact
(
  id                     CHAR(36) PRIMARY KEY DEFAULT uuid_generate_v4(),
  organization_id        CHAR(36)             DEFAULT NULL REFERENCES organization (id) ON DELETE CASCADE ON UPDATE CASCADE,
  service_at_location_id CHAR(36)             DEFAULT NULL REFERENCES service_at_location (id) ON DELETE CASCADE ON UPDATE CASCADE,
  name                   VARCHAR              DEFAULT NULL,
  title                  VARCHAR              DEFAULT NULL,
  department             VARCHAR              DEFAULT NULL,
  email                  VARCHAR              DEFAULT NULL
);
package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Location {

  @JsonProperty("organization_id")
  private String organizationId;

  @JsonProperty("alternate_name")
  private String alternateName;

  private String id;
  private String name;
  private String description;
  private String transportation;
  private String latitude;
  private String longitude;

  public Location(ResultSet rs) throws SQLException {
    this.id = rs.getString("id");
    this.organizationId = rs.getString("organization_id");
    this.name = rs.getString("name");
    this.alternateName = rs.getString("alternate_name");
    this.description = rs.getString("description");
    this.transportation = rs.getString("transportation");
    this.latitude = rs.getString("latitude");
    this.longitude = rs.getString("longitude");
  }

  public String getId() {
    return id;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public String getName() {
    return name;
  }

  public String getAlternateName() {
    return alternateName;
  }

  public String getDescription() {
    return description;
  }

  public String getTransportation() {
    return transportation;
  }

  public String getLatitude() {
    return latitude;
  }

  public String getLongitude() {
    return longitude;
  }
}

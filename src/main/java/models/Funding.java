package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Funding {

  @JsonProperty("organization_id")
  private String organizationId;

  @JsonProperty("service_id")
  private String serviceId;

  private String id;
  private String source;

  public Funding(ResultSet rs) throws SQLException {
    this.id = rs.getString("id");
    this.organizationId = rs.getString("organization_id");
    this.serviceId = rs.getString("service_id");
    this.source = rs.getString("source");
  }

  public String getId() {
    return id;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public String getServiceId() {
    return serviceId;
  }

  public String getSource() {
    return source;
  }
}

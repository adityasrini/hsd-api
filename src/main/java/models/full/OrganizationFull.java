package models.full;

import java.util.ArrayList;
import java.util.List;
import models.Contact;
import models.Funding;
import models.Location;
import models.Organization;
import models.Phone;
import models.Program;
import models.Service;

public class OrganizationFull extends Organization {

  private List<Contact> contacts = new ArrayList<>();
  private List<Location> locations = new ArrayList<>();
  private List<Service> services = new ArrayList<>();
  private List<Program> programs = new ArrayList<>();
  private List<Funding> funding = new ArrayList<>();
  private List<Phone> phones = new ArrayList<>();

  public OrganizationFull(Organization organization) {
    super(organization);
  }

  public List<Contact> getContacts() {
    return contacts;
  }

  public void setContacts(List<Contact> contact) {
    if (contact != null) {
      this.contacts = contact;
    }
  }

  public List<Location> getLocations() {
    return locations;
  }

  public void setLocations(List<Location> location) {
    if (location != null) {
      this.locations = location;
    }
  }

  public List<Service> getServices() {
    return services;
  }

  public void setServices(List<Service> services) {
    if (services != null) {
      this.services = services;
    }
  }

  public List<Program> getPrograms() {
    return programs;
  }

  public void setPrograms(List<Program> programs) {
    if (programs != null) {
      this.programs = programs;
    }
  }

  public List<Funding> getFunding() {
    return funding;
  }

  public void setFunding(List<Funding> funding) {
    if (funding != null) {
      this.funding = funding;
    }
  }

  public List<Phone> getPhones() {
    return phones;
  }

  public void setPhones(List<Phone> phones) {
    if (phones != null) {
      this.phones = phones;
    }
  }
}

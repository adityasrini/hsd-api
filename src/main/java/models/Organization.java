package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Organization {

  @JsonProperty("alternate_name")
  private String alternateName;

  @JsonProperty("tax_status")
  private String taxStatus;

  @JsonProperty("tax_id")
  private String taxId;

  @JsonProperty("year_incorporated")
  private String yearIncorporated;

  @JsonProperty("legal_status")
  private String legalStatus;

  private String id;
  private String name;
  private String description;
  private String email;
  private String url;

  public Organization(ResultSet rs) throws SQLException {
    this.id = rs.getString("id");
    this.name = rs.getString("name");
    this.alternateName = rs.getString("alternate_name");
    this.description = rs.getString("description");
    this.email = rs.getString("email");
    this.url = rs.getString("url");
    this.taxStatus = rs.getString("tax_status");
    this.taxId = rs.getString("tax_id");
    this.yearIncorporated = rs.getString("year_incorporated");
    this.legalStatus = rs.getString("legal_status");
  }

  public Organization(Organization organization) {
    this.id = organization.getId();
    this.name = organization.getName();
    this.alternateName = organization.getAlternateName();
    this.description = organization.getDescription();
    this.email = organization.getEmail();
    this.url = organization.getUrl();
    this.taxStatus = organization.getTaxStatus();
    this.taxId = organization.getTaxId();
    this.yearIncorporated = organization.getYearIncorporated();
    this.legalStatus = organization.getLegalStatus();
  }

  public String getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getAlternateName() {
    return alternateName;
  }

  public String getDescription() {
    return description;
  }

  public String getEmail() {
    return email;
  }

  public String getUrl() {
    return url;
  }

  public String getTaxStatus() {
    return taxStatus;
  }

  public String getTaxId() {
    return taxId;
  }

  public String getYearIncorporated() {
    return yearIncorporated;
  }

  public String getLegalStatus() {
    return legalStatus;
  }
}

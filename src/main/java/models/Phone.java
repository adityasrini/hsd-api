package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Phone {

  @JsonProperty("location_id")
  private String locationId;

  @JsonProperty("service_id")
  private String serviceId;

  @JsonProperty("organization_id")
  private String organizationId;

  @JsonProperty("contact_id")
  private String contactId;

  @JsonProperty("service_at_location_id")
  private String serviceAtLocationId;

  private String id;
  private String number;
  private String extension;
  private String type;
  private String language;
  private String description;

  public Phone(ResultSet rs) throws SQLException {
    this.id = rs.getString("id");
    this.locationId = rs.getString("location_id");
    this.serviceId = rs.getString("service_id");
    this.organizationId = rs.getString("organization_id");
    this.contactId = rs.getString("contact_id");
    this.serviceAtLocationId = rs.getString("service_at_location_id");
    this.number = rs.getString("number");
    this.extension = rs.getString("extension");
    this.type = rs.getString("type");
    this.language = rs.getString("language");
    this.description = rs.getString("description");
  }

  public String getId() {
    return id;
  }

  public String getLocationId() {
    return locationId;
  }

  public String getServiceId() {
    return serviceId;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public String getContactId() {
    return contactId;
  }

  public String getServiceAtLocationId() {
    return serviceAtLocationId;
  }

  public String getNumber() {
    return number;
  }

  public String getExtension() {
    return extension;
  }

  public String getType() {
    return type;
  }

  public String getLanguage() {
    return language;
  }

  public String getDescription() {
    return description;
  }
}

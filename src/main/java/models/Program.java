package models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Program {

  @JsonProperty("organization_id")
  private String organizationId;

  @JsonProperty("alternate_name")
  private String alternateName;

  private String id;
  private String name;

  public Program(String id, String organizationId,
      String name, String alternateName) {
    this.id = id;
    this.organizationId = organizationId;
    this.name = name;
    this.alternateName = alternateName;
  }

  public String getId() {
    return id;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public String getName() {
    return name;
  }

  public String getAlternateName() {
    return alternateName;
  }
}
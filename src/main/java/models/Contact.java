package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Contact {

  @JsonProperty("organization_id")
  private String organizationId;

  @JsonProperty("service_id")
  private String serviceId;

  @JsonProperty("service_at_location_id")
  private String serviceAtLocationId;

  private String id;
  private String name;
  private String title;
  private String department;
  private String email;

  public Contact(ResultSet rs) throws SQLException {
    this.id = rs.getString("id");
    this.organizationId = rs.getString("organization_id");
    this.serviceId =   rs.getString("service_id");
    this.serviceAtLocationId =   rs.getString("service_at_location_id");
    this.name =   rs.getString("name");
    this.title =  rs.getString("title");
    this.department =   rs.getString("department");
    this.email = rs.getString("email");
  }

  public String getId() {
    return id;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public String getServiceId() {
    return serviceId;
  }

  public String getServiceAtLocationId() {
    return serviceAtLocationId;
  }

  public String getName() {
    return name;
  }

  public String getTitle() {
    return title;
  }

  public String getDepartment() {
    return department;
  }

  public String getEmail() {
    return email;
  }
}

package models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Service {

  @JsonProperty("organization_id")
  private String organizationId;

  @JsonProperty("program_id")
  private String programId;

  @JsonProperty("location_id")
  private String locationId;

  @JsonProperty("alternate_name")
  private String alternateName;

  @JsonProperty("interpretation_services")
  private String interpretationServices;

  @JsonProperty("application_process")
  private String applicationProcess;

  @JsonProperty("wait_time")
  private String waitTime;

  private String id;
  private String name;
  private String description;
  private String url;
  private String email;
  private String status;
  private String fees;
  private String accreditations;
  private String licenses;

  public Service(ResultSet rs) throws SQLException {
    this.id = rs.getString("id");
    this.organizationId =   rs.getString("organization_id");
    this.programId = rs.getString("program_id");
    this.locationId =   rs.getString("location_id");
    this.name =  rs.getString("name");
    this.alternateName = rs.getString("alternate_name");
    this.description =   rs.getString("description");
    this.url = rs.getString("url");
    this.email =  rs.getString("email");
    this.status = rs.getString("status");
    this.interpretationServices = rs.getString("interpretation_services");
    this.applicationProcess = rs.getString("application_process");
    this.waitTime =  rs.getString("wait_time");
    this.fees =   rs.getString("fees");
    this.accreditations =  rs.getString("accreditations");
    this.licenses = rs.getString("licenses");
  }

  public String getId() {
    return id;
  }

  public String getOrganizationId() {
    return organizationId;
  }

  public String getProgramId() {
    return programId;
  }

  public String getLocationId() {
    return locationId;
  }

  public String getName() {
    return name;
  }

  public String getAlternateName() {
    return alternateName;
  }

  public String getDescription() {
    return description;
  }

  public String getUrl() {
    return url;
  }

  public String getEmail() {
    return email;
  }

  public String getStatus() {
    return status;
  }

  public String getInterpretationServices() {
    return interpretationServices;
  }

  public String getApplicationProcess() {
    return applicationProcess;
  }

  public String getWaitTime() {
    return waitTime;
  }

  public String getFees() {
    return fees;
  }

  public String getAccreditations() {
    return accreditations;
  }

  public String getLicenses() {
    return licenses;
  }
}

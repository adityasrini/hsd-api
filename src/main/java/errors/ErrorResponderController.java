package errors;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.http.hateoas.Link;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Controller
public class ErrorResponderController {

  private static final Logger LOG = LoggerFactory.getLogger(ErrorResponderController.class);

  @Error(global = true)
  public HttpResponse<JsonError> illegalQueryParamsException(HttpRequest request,
      IllegalQueryParametersException e) {
    JsonError jsonError = new JsonError("Illegal query parameters were entered.")
            .link(Link.SELF, Link.of(request.getUri()));
    return HttpResponse.<JsonError>status(HttpStatus.UNPROCESSABLE_ENTITY).body(jsonError);
  }

  @Error(global = true)
  public HttpResponse<JsonError> internalServerError(HttpRequest request, Throwable e) {
    JsonError jsonError = new JsonError("A server error occurred. "
        + "Sorry for the inconvenience!")
        .link(Link.SELF, Link.of(request.getUri()));
    LOG.error("\nMessage: " + e.getMessage() + "\nStacktrace:", e);
    return HttpResponse.<JsonError>serverError().body(jsonError);
  }

  @Error(status = HttpStatus.NOT_FOUND, global = true)
  public HttpResponse<JsonError> notFound(HttpRequest request) {
    JsonError jsonError = new JsonError("Resource not found")
        .link(Link.SELF, Link.of(request.getUri()));

    return HttpResponse.<JsonError>notFound().body(jsonError);
  }
}

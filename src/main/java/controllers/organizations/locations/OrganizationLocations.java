package controllers.organizations.locations;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import java.util.List;
import javax.annotation.security.PermitAll;
import models.Location;
import service.organizations.OrganizationLocationServ;

@Controller("/organizations/{organizationId}")
public class OrganizationLocations {

  private OrganizationLocationServ organizationLocationServ;

  public OrganizationLocations(OrganizationLocationServ organizationLocationServ) {
    this.organizationLocationServ = organizationLocationServ;
  }

  @Get("/locations")
  @PermitAll
  public List<Location> indexLocations(String organizationId) {
    return organizationLocationServ.getLocations(organizationId);
  }

  @Get("/locations/{locationId}")
  @PermitAll
  public List<Location> showLocation(String organizationId, String locationId) {
    return organizationLocationServ
        .getOneLocation(organizationId, locationId);
  }
}

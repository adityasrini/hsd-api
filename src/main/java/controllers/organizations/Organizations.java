package controllers.organizations;

import errors.IllegalQueryParametersException;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.QueryValue;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Nullable;
import javax.annotation.security.PermitAll;
import models.Organization;
import models.full.OrganizationFull;
import service.OrganizationsFullServ;
import service.organizations.OrganizationServ;

@Controller("/organizations")
public class Organizations {

  private OrganizationServ organizationService;
  private OrganizationsFullServ organizationsFullServ;

  public Organizations(OrganizationServ organizationService,
      OrganizationsFullServ organizationsFullServ) {
    this.organizationService = organizationService;
    this.organizationsFullServ = organizationsFullServ;
  }

  @Get
  @PermitAll
  public List<Organization> index(
      @QueryValue(defaultValue = "id") String query,
      @QueryValue(defaultValue = "%") String queries,
      @QueryValue(defaultValue = "name") String sort_by,
      @QueryValue(defaultValue = "ASC") String order,
      @QueryValue @Nullable String per_page) {
    var orderUpperCase = order.toUpperCase();
    var parsedPerPage = whiteListParams(query, sort_by, orderUpperCase, per_page);
    return organizationService
        .getOrganizations(query, queries, sort_by, orderUpperCase, parsedPerPage);
  }

  @Get("/full/")
  @PermitAll
  public List<OrganizationFull> indexFull(
      @QueryValue(defaultValue = "id") String query,
      @QueryValue(defaultValue = "%") String queries,
      @QueryValue(defaultValue = "name") String sort_by,
      @QueryValue(defaultValue = "ASC") String order,
      @QueryValue @Nullable String per_page) {
    var orderUpperCase = order.toUpperCase();
    var parsedPerPage = whiteListParams(query, sort_by, orderUpperCase, per_page);
    return organizationsFullServ
        .getOrganizations(query, queries, sort_by, orderUpperCase, parsedPerPage);
  }

  @Get("/full/{organizationId}")
  @PermitAll
  public OrganizationFull showFull(String organizationId) {
    return organizationsFullServ.getOneOrganization(organizationId);
  }

  @Get("/{organizationId}")
  @PermitAll
  public List<Organization> show(String organizationId) {
    return organizationService.getOneOrganization(organizationId);
  }

  private static int whiteListParams(String query, String sortBy, String order, String perPage) {
    try {
      var perPageParsedInteger = perPage == null ? 100 : Integer.parseInt(perPage);
      if (organizationOrderWhitelist(order) && organizationQueryWhitelist(query)
          && organizationSortByWhitelist(sortBy)) {
        return perPageParsedInteger > 100 ? 100 : perPageParsedInteger;
      } else {
        throw new IllegalQueryParametersException();
      }
    } catch (NumberFormatException e) {
      throw new IllegalQueryParametersException();
    }
  }

  private static boolean organizationQueryWhitelist(String query) {
    return Arrays.asList("id", "name", "alternate_name").contains(query);
  }

  private static boolean organizationSortByWhitelist(String sortBy) {
    return Arrays
        .asList("id", "name", "alternate_name", "description", "email", "url", "tax_status",
            "tax_id", "year_incorporated", "legal_status").contains(sortBy);
  }

  private static boolean organizationOrderWhitelist(String order) {
    return Arrays.asList("DESC", "ASC").contains(order);
  }
}

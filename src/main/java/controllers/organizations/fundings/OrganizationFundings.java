package controllers.organizations.fundings;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import java.util.List;
import javax.annotation.security.PermitAll;
import models.Funding;
import service.organizations.OrganizationsFundingServ;

@Controller("/organizations/{organizationId}")
public class OrganizationFundings {

  private OrganizationsFundingServ organizationsFundingServ;

  public OrganizationFundings(OrganizationsFundingServ organizationsFundingServ) {
    this.organizationsFundingServ = organizationsFundingServ;
  }

  @Get("/fundings")
  @PermitAll
  public List<Funding> indexFunding(String organizationId) {
    return organizationsFundingServ.getAllFunding(organizationId);
  }

  @Get("/fundings/{fundingId}")
  @PermitAll
  public List<Funding> showFunding(String organizationId, String fundingId) {
    return organizationsFundingServ
        .getOneFunding(organizationId, fundingId);
  }
}

package controllers.organizations.programs;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import java.util.List;
import javax.annotation.security.PermitAll;
import models.Program;
import models.Service;
import service.organizations.OrganizationsProgramServ;

@Controller("/organizations/{organizationId}")
public class OrganizationPrograms {

  private OrganizationsProgramServ organizationsProgramServ;

  public OrganizationPrograms(OrganizationsProgramServ organizationsProgramServ) {
    this.organizationsProgramServ = organizationsProgramServ;
  }

  @Get("/programs")
  @PermitAll
  public List<Program> indexPrograms(String organizationId) {
    return organizationsProgramServ.getPrograms(organizationId);
  }

  @Get("/programs/{programId}")
  @PermitAll
  public List<Program> showProgram(String organizationId, String programId) {
    return organizationsProgramServ
        .getOneProgram(organizationId, programId);
  }

  @Get("/programs/{programId}/services")
  @PermitAll
  public List<Service> indexServicesByProgramId(String organizationId, String programId) {
    return organizationsProgramServ.getServicesByProgramId(organizationId, programId);
  }

  @Get("/programs/{programId}/services/{serviceId}")
  @PermitAll
  public List<Service> showServicesByProgramId(String organizationId, String programId,
      String serviceId) {
    return organizationsProgramServ
        .getOneServiceByProgramId(organizationId, programId, serviceId);
  }
}

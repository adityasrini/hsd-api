package controllers.organizations.services;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import java.util.List;
import javax.annotation.security.PermitAll;
import models.Service;
import service.organizations.OrganizationsServiceServ;

@Controller("/organizations/{organizationId}")
public class OrganizationServices {

  private OrganizationsServiceServ organizationServiceService;

  public OrganizationServices(OrganizationsServiceServ organizationsServiceServ) {
    this.organizationServiceService = organizationsServiceServ;
  }

  @Get("/services")
  @PermitAll
  public List<Service> indexServices(String organizationId) {
    return organizationServiceService.getServices(organizationId);
  }

  @Get("/services/{serviceId}")
  @PermitAll
  public List<Service> showServices(String organizationId, String serviceId) {
    return organizationServiceService
        .getOneService(organizationId, serviceId);
  }
}

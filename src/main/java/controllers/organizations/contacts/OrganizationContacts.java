package controllers.organizations.contacts;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import java.util.List;
import javax.annotation.security.PermitAll;
import models.Contact;
import service.organizations.OrganizationContactsServ;

@Controller("/organizations/{organizationId}")
public class OrganizationContacts {

  private OrganizationContactsServ organizationContactsServ;

  public OrganizationContacts(OrganizationContactsServ organizationContactsServ) {
    this.organizationContactsServ = organizationContactsServ;
  }

  @Get("/contacts")
  @PermitAll
  public List<Contact> indexContacts(String organizationId) {
    return organizationContactsServ.getContacts(organizationId);
  }

  @Get("/contacts/{contactId}")
  @PermitAll
  public List<Contact> showContact(String organizationId, String contactId) {
    return organizationContactsServ
        .getOneContact(organizationId, contactId);
  }
}

package controllers.organizations.phones;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import java.util.List;
import javax.annotation.security.PermitAll;
import models.Phone;
import service.organizations.OrganizationsPhonesServ;

@Controller("/organizations/{organizationId}")
public class OrganizationPhones {

  private OrganizationsPhonesServ organizationsPhonesServ;

  public OrganizationPhones(OrganizationsPhonesServ organizationsPhonesServ) {
    this.organizationsPhonesServ = organizationsPhonesServ;
  }

  @Get("/phones")
  @PermitAll
  public List<Phone> indexPhones(String organizationId) {
    return organizationsPhonesServ.getPhones(organizationId);
  }

  @Get("/phones/{phoneId}")
  @PermitAll
  public List<Phone> showPhone(String organizationId, String phoneId) {
    return organizationsPhonesServ
        .getOnePhone(organizationId, phoneId);
  }
}

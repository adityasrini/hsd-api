package controllers;

import io.micronaut.context.annotation.Replaces;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.security.handlers.ForbiddenRejectionUriProvider;
import io.micronaut.security.handlers.RedirectRejectionHandler;
import io.micronaut.security.handlers.RedirectRejectionHandlerConfiguration;
import io.micronaut.security.handlers.UnauthorizedRejectionUriProvider;
import io.reactivex.Flowable;
import javax.inject.Singleton;
import org.reactivestreams.Publisher;

@Singleton
@Replaces(RedirectRejectionHandler.class)
public class UnauthorizedRejectionHandler extends RedirectRejectionHandler {

  public UnauthorizedRejectionHandler(
      UnauthorizedRejectionUriProvider unauthorizedRejectionUriProvider,
      ForbiddenRejectionUriProvider forbiddenRejectionUriProvider,
      RedirectRejectionHandlerConfiguration redirectRejectionHandlerConfiguration) {
    super(unauthorizedRejectionUriProvider, forbiddenRejectionUriProvider,
        redirectRejectionHandlerConfiguration);
  }

  @Override
  public Publisher<MutableHttpResponse<?>> reject(HttpRequest<?> request, boolean forbidden) {
    return Flowable.fromPublisher(super.reject(request, forbidden))
        .map(r -> HttpResponse
            .unauthorized()
            .body(new JsonError("Not authorized to make this request")));
  }

  @Override
  protected boolean shouldHandleRequest(HttpRequest<?> request) {
    return false;
  }
}

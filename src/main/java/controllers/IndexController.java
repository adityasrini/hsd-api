package controllers;

import io.micronaut.core.util.CollectionUtils;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.views.View;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.security.PermitAll;

@Controller
public class IndexController {

  @Get
  @PermitAll
  public HashMap<String, String> indexGet() {
    var linkHashMap = new HashMap<String, String>();
    linkHashMap.put("List of organizations", "/organizations/");
    linkHashMap.put("Full data about organizations", "/organizations/full/");
    linkHashMap.put("Organization by id", "/organizations/{organization_id}/");
    return linkHashMap;
  }

  @Get("/authenticated-user")
  @Secured(SecurityRule.IS_AUTHENTICATED)
  public Map<String, Object> getAuthenticatedUserDetails(Authentication authentication) {
    return authentication.getAttributes();
  }

  @Get("/login-success")
  @View("login-success")
  @Secured(SecurityRule.IS_AUTHENTICATED)
  public HttpResponse loginSuccess() {
    return HttpResponse.ok(CollectionUtils.mapOf("targetOrigin", System.getenv("TARGET_ORIGIN")));
  }
}

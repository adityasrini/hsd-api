package service.organizations;

import java.util.List;
import javax.inject.Singleton;
import javax.sql.DataSource;
import models.Program;
import models.Service;
import org.springframework.jdbc.core.JdbcTemplate;

@Singleton
public class OrganizationsProgramServ {

  private JdbcTemplate jdbcTemplate;

  public OrganizationsProgramServ(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
  }

  public List<Program> getPrograms(String organizationId) {
    return programQuery(organizationId, "%");
  }

  public List<Program> getOneProgram(String organizationId,
      String programId) {
    return programQuery(organizationId, programId);
  }

  public List<Service> getServicesByProgramId(String organizationId, String programId) {
    return serviceQuery(organizationId, programId, "%");
  }

  public List<Service> getOneServiceByProgramId(String organizationId, String programId,
      String serviceId) {
    return serviceQuery(organizationId, programId, serviceId);
  }

  private List<Program> programQuery(String organizationId, String programId) {
    String statement =
        "SELECT p.* FROM program p WHERE p.organization_id=? AND p.id LIKE ? ";
    var resultArray = jdbcTemplate
        .query(statement, new Object[]{organizationId, programId},
            (rs, row) -> new Program(rs.getString("id"),
                rs.getString("organization_id"),
                rs.getString("name"),
                rs.getString("alternate_name")
            ));

    return resultArray.size() == 0 ? null : resultArray;
  }

  private List<Service> serviceQuery(String organizationId, String programId, String serviceId) {
    String statement =
        "SELECT s.*, sal.location_id FROM service s INNER JOIN program p ON p.id=s.program_id"
            + " INNER JOIN service_at_location sal ON sal.service_id=s.id "
            + "WHERE p.organization_id=? AND p.id=? AND s.id LIKE ? ";
    var resultArray = jdbcTemplate
        .query(statement, new Object[]{organizationId, programId, serviceId},
            (rs, row) -> new Service(rs));
    return resultArray.size() == 0 ? null : resultArray;
  }
}

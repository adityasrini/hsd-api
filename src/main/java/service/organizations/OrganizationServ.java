package service.organizations;

import java.util.List;
import javax.inject.Singleton;
import javax.sql.DataSource;
import models.Organization;
import org.springframework.jdbc.core.JdbcTemplate;

@Singleton
public class OrganizationServ {

  private JdbcTemplate jdbcTemplate;

  public OrganizationServ(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
  }

  public List<Organization> getOneOrganization(String id) {
    return organizationQuery("id", id, "name", "ASC", 1);
  }

  public List<Organization> getOrganizations(String query, String queries, String sortBy,
      String order, int perPage) {
    return organizationQuery(query, "%" + queries.replace(",", "%|") + "%", sortBy, order, perPage);
  }

  private List<Organization> organizationQuery(String query, String queries, String sortBy,
      String order,
      int perPage) {
    var statement = "SELECT * FROM organization WHERE " + query + " ILIKE ? ORDER BY " + sortBy
        + " " + order + " LIMIT " + perPage;
    var resultArray = jdbcTemplate
        .query(statement, new Object[]{queries},
            (rs, row) -> new Organization(rs));
    return resultArray.size() == 0 ? null : resultArray;
  }
}

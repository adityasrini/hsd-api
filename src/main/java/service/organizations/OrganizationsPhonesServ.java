package service.organizations;

import java.util.List;
import javax.inject.Singleton;
import javax.sql.DataSource;
import models.Phone;
import org.springframework.jdbc.core.JdbcTemplate;

@Singleton
public class OrganizationsPhonesServ {

  private JdbcTemplate jdbcTemplate;
  private String statement;

  public OrganizationsPhonesServ(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
    this.statement =
        "SELECT p.*, sal.location_id, sal.service_id FROM phone p "
            + "INNER JOIN service_at_location sal ON p.service_at_location_id = sal.id "
            + "WHERE p.organization_id=? AND p.id LIKE ? ";
  }

  public List<Phone> getPhones(String organizationId) {

    return phoneQuery(organizationId, "%");
  }

  public List<Phone> getOnePhone(String organizationId,
      String phoneId) {

    return phoneQuery(organizationId, phoneId);
  }

  private List<Phone> phoneQuery(String organizationId, String phoneId) {
    var resultArray = jdbcTemplate
        .query(statement, new Object[]{organizationId, phoneId},
            (rs, row) -> new Phone(rs));
    return resultArray.size() == 0 ? null : resultArray;
  }
}

package service.organizations;

import java.util.List;
import javax.inject.Singleton;
import javax.sql.DataSource;
import models.Location;
import org.springframework.jdbc.core.JdbcTemplate;

@Singleton
public class OrganizationLocationServ {

  private JdbcTemplate jdbcTemplate;
  private String statement;

  public OrganizationLocationServ(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
    this.statement =
        "SELECT l.* FROM location l, organization o WHERE o.id=? AND l.id LIKE ? "
            + "AND l.organization_id = o.id";
  }

  public List<Location> getLocations(String organizationId) {
    return locationQuery(organizationId, "%");
  }

  public List<Location> getOneLocation(String organizationId,
      String locationId) {
    return locationQuery(organizationId, locationId);
  }

  private List<Location> locationQuery(String organizationId, String locationId) {
    var resultArray = jdbcTemplate
        .query(statement, new Object[]{organizationId, locationId},
            (rs, row) -> new Location(rs));
    return resultArray.size() == 0 ? null : resultArray;
  }
}

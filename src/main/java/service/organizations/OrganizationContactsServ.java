package service.organizations;

import java.util.List;
import javax.inject.Singleton;
import javax.sql.DataSource;
import models.Contact;
import org.springframework.jdbc.core.JdbcTemplate;

@Singleton
public class OrganizationContactsServ {

  private JdbcTemplate jdbcTemplate;
  private String statement;

  public OrganizationContactsServ(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
    this.statement =
        "SELECT c.*, sal.service_id FROM organization o, contact c INNER JOIN service_at_location"
            + " sal ON sal.id=c.service_at_location_id WHERE o.id=? AND c.id LIKE ? "
            + "AND c.organization_id = o.id";
  }

  public List<Contact> getContacts(String organizationId) {
    return contactQuery(organizationId, "%");
  }

  public List<Contact> getOneContact(String organizationId,
      String contactId) {
    return contactQuery(organizationId, contactId);
  }

  private List<Contact> contactQuery(String organizationId, String contactId) {
    var resultArray = jdbcTemplate
        .query(statement, new Object[]{organizationId, contactId},
            (rs, row) -> new Contact(rs));
    return resultArray.size() == 0 ? null : resultArray;
  }
}

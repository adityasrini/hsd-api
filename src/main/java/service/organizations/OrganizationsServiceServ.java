package service.organizations;

import java.util.List;
import javax.inject.Singleton;
import javax.sql.DataSource;
import models.Service;
import org.springframework.jdbc.core.JdbcTemplate;

@Singleton
public class OrganizationsServiceServ {

  private JdbcTemplate jdbcTemplate;
  private String statement;

  public OrganizationsServiceServ(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
    this.statement =
        "SELECT s.*, sal.location_id FROM service s INNER JOIN service_at_location sal "
            + "ON s.id = sal.service_id AND s.organization_id=? AND s.id LIKE ? ";
  }

  public List<Service> getServices(String organizationId) {
    return serviceQuery(organizationId, "%");
  }

  public List<Service> getOneService(String organizationId,
      String serviceId) {
    return serviceQuery(organizationId, serviceId);
  }

  private List<Service> serviceQuery(String organizationId, String serviceId) {

    var resultArray = jdbcTemplate
        .query(statement, new Object[]{organizationId, serviceId},
            (rs, row) -> new Service(rs));
    return resultArray.size() == 0 ? null : resultArray;
  }
}

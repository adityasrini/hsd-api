package service.organizations;

import java.util.List;
import javax.inject.Singleton;
import javax.sql.DataSource;
import models.Funding;
import org.springframework.jdbc.core.JdbcTemplate;

@Singleton
public class OrganizationsFundingServ {

  private JdbcTemplate jdbcTemplate;
  private String statement;

  public OrganizationsFundingServ(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
    this.statement =
        "SELECT f.* FROM funding f WHERE f.organization_id=? AND f.id LIKE ? ";
  }

  public List<Funding> getAllFunding(String organizationId) {
    return fundingQuery(organizationId, "%");
  }

  public List<Funding> getOneFunding(String organizationId,
      String fundingId) {
    return fundingQuery(organizationId, fundingId);
  }

  private List<Funding> fundingQuery(String organizationId, String fundingId) {
    var resultArray = jdbcTemplate
        .query(statement, new Object[]{organizationId, fundingId},
            (rs, row) -> new Funding(rs));
    return resultArray.size() == 0 ? null : resultArray;
  }
}

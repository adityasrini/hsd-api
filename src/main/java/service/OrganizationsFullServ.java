package service;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Singleton;
import javax.sql.DataSource;
import models.full.OrganizationFull;
import org.springframework.jdbc.core.JdbcTemplate;
import service.organizations.OrganizationContactsServ;
import service.organizations.OrganizationLocationServ;
import service.organizations.OrganizationServ;
import service.organizations.OrganizationsFundingServ;
import service.organizations.OrganizationsPhonesServ;
import service.organizations.OrganizationsProgramServ;
import service.organizations.OrganizationsServiceServ;

@Singleton
public class OrganizationsFullServ {

  private OrganizationServ organizationService;
  private OrganizationContactsServ organizationContactsServ;
  private OrganizationLocationServ organizationLocationServ;
  private OrganizationsServiceServ organizationsServiceServ;
  private OrganizationsProgramServ organizationsProgramServ;
  private OrganizationsFundingServ organizationsFundingServ;
  private OrganizationsPhonesServ organizationsPhonesServ;
  private JdbcTemplate jdbcTemplate;

  public OrganizationsFullServ(DataSource dataSource, OrganizationServ organizationService,
      OrganizationContactsServ organizationContactsServ,
      OrganizationLocationServ organizationLocationServ,
      OrganizationsServiceServ organizationsServiceServ,
      OrganizationsProgramServ organizationsProgramServ,
      OrganizationsFundingServ organizationsFundingServ,
      OrganizationsPhonesServ organizationsPhonesServ) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
    this.organizationService = organizationService;
    this.organizationContactsServ = organizationContactsServ;
    this.organizationLocationServ = organizationLocationServ;
    this.organizationsServiceServ = organizationsServiceServ;
    this.organizationsProgramServ = organizationsProgramServ;
    this.organizationsFundingServ = organizationsFundingServ;
    this.organizationsPhonesServ = organizationsPhonesServ;
  }

  public List<OrganizationFull> getOrganizations(String query, String queries,
      String sortBy, String order, int perPage) {
    var statementString = "SELECT id FROM organization WHERE " + query + " ILIKE ? ORDER BY "
        + sortBy + " " + order + " LIMIT " + perPage;
    queries = "%" + queries.replace(",", "%|") + "%";

    List<String> organizationIds = jdbcTemplate
        .query(statementString, new Object[]{queries}, (rs, rowNum) -> rs.getString(1));

    var listOfOrganizationFull = new ArrayList<OrganizationFull>();

    for (String organizationId : organizationIds) {
      listOfOrganizationFull.add(getOneOrganization(organizationId));
    }

    return listOfOrganizationFull;
  }

  public OrganizationFull getOneOrganization(String organizationId) {

    var organizationList = organizationService.getOneOrganization(organizationId);
    if (organizationList == null) {
      return null;
    }

    var organizationFull = new OrganizationFull(organizationList.get(0));
    var contactList = organizationContactsServ.getContacts(organizationId);
    var locationList = organizationLocationServ.getLocations(organizationId);
    var serviceList = organizationsServiceServ.getServices(organizationId);
    var programList = organizationsProgramServ.getPrograms(organizationId);
    var fundingList = organizationsFundingServ.getAllFunding(organizationId);
    var phoneList = organizationsPhonesServ.getPhones(organizationId);

    organizationFull.setLocations(locationList);
    organizationFull.setContacts(contactList);
    organizationFull.setServices(serviceList);
    organizationFull.setPrograms(programList);
    organizationFull.setFunding(fundingList);
    organizationFull.setPhones(phoneList);

    return organizationFull;
  }
}
